const sequelize = require('../config/sequelize')

const User = require('../model/userModel')
const Depart = require('../model/departModel')
const Exam = require('../model/examModel')
const Repo = require('../model/repoModel')
const Paper = require('../model/paperModel')
const PaperQu = require('../model/paperQuModel')
const Qu = require('../model/quModel')
const QuAns = require('../model/quAnsModel')
const QuRepo = require('../model/quRepoModel')
const PaperQuAns = require('../model/paperQuAnsModel')
const UserBook = require('../model/userBookModel')
const ExamRepo = require('../model/examRepoModel')
const UserExam = require('../model/userExamModel')
const Role = require('../model/roleModel');



(async ()=>{
  let res = await Repo.findAll({
    include:ExamRepo
  })
  console.log(res)
})();
