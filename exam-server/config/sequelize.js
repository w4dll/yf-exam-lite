const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('exam1', 'root', 'dll123', {
  host: 'localhost',
  dialect: 'mysql',
  timezone: '+08:00',  // 时区调整
  define: {
    freezeTableName: true,

    timestamps: true,          // 启用默认时间戳，默认true
    createdAt: 'create_time',  // 自定义创建时间字段
    updatedAt: 'update_time',  // 自定义更新时间字段
  },
  dialectOptions: {
    charset: 'utf8mb4',
    dateStrings: true,
    typeCast: true,
  },
});

// 数据库连接测试
// try {
//   sequelize.authenticate();
//   console.log('Connection has been established successfully.');
// } catch (error) {
//   console.error('Unable to connect to the database:', error);
// }

module.exports = sequelize;
