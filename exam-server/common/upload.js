const multer = require('multer');

// 保存form-data数据
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
    //文件保存路径
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime() + '_' + file.originalname);
    //存储文件名
  },
});
const upload = multer({ storage: storage });

module.exports = upload
