module.exports = (req, res, next) => {
    res.sendResult = (data, code, message) => {
        res.json({
            "data": data,
            "meta": {
                "msg": message,
                "status": code
            }
        })
    }
    next()
}