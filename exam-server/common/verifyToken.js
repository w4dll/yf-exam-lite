const { verifyToken } = require('../utils/token');
const { SimpleData } = require('./returnDataFormat');

module.exports.verifyUser = async function (req, res, next) {
  console.log(req.path, '-------verify user')
  const token = req.headers.token;
  if (
    req.path == '/login' ||
    req.path == '/reg'
  ) {
    return next();
  }

  if (!token) {
    return res.send({ ...SimpleData(500, 'token不存在或错误'), token: false });
  } else {
    try {
      let res = await verifyToken(token)
      console.log('-------token verify success')
      next()
      // return next()
    } catch (error) {
      return res.send(SimpleData(502, 'token验证错误'));
    }
  }
};

module.exports.verifyAdmin = async (req, res, next) => {
  const token = req.headers.authorization;
  if (req.path == '/login' || req.path == '/register') {
    return next();
  }
  if (!token) {
    return res.send({ ...SimpleData(500, 'token不存在或错误'), token: false });
  } else {
    try {
      let { data: user } = await verifyToken(token);
      if (user.userLevel > 1) {
        next();
      } else {
        res.send(SimpleData(503, '用户非管理员'));
      }
    } catch (error) {
      res.send(SimpleData(502, 'token验证错误'));
    }
  }
};

module.exports.verifyToken = async function (req, res, next) {
  console.log('-------token verify')
  const token = req.headers.token;
  if (!token) {
    return res.send({ ...SimpleData(500, 'token不存在或错误'), token: false });
  } else {
    try {
      await verifyToken(token);
      next();
    } catch (error) {
      res.send(SimpleData(502, 'token验证错误'));
    }
  }
};
