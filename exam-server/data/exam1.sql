/*
 Navicat Premium Data Transfer

 Source Server         : sql_db
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : exam1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 09/02/2022 21:48:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for el_exam
-- ----------------------------
DROP TABLE IF EXISTS `el_exam`;
CREATE TABLE `el_exam`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `open_type` int(0) NULL DEFAULT NULL,
  `join_type` int(0) NULL DEFAULT NULL,
  `level` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  `time_limit` int(0) NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `total_score` int(0) NULL DEFAULT NULL,
  `total_time` int(0) NULL DEFAULT NULL,
  `qualify_score` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam
-- ----------------------------
INSERT INTO `el_exam` VALUES ('0aff40fb-2147-4364-983a-41013dc6aee8', '大学物理', '测试描述11', 1, 1, 1, 0, 1, '2022-01-19', '2022-01-21', 11, 50, 10, '2022-01-18 12:24:09', '2022-01-22 21:42:31');
INSERT INTO `el_exam` VALUES ('1a0f77f0-3f4c-406f-b892-d679735829f1', '大学英语', '  学号英语才能骗老外', 1, 1, 1, 0, 1, '2022-01-18', '2022-02-22', 12, 12, 1, '2022-01-18 13:03:17', '2022-01-22 21:42:53');
INSERT INTO `el_exam` VALUES ('1bcc2706-f734-4fd5-9c99-80971cf97c05', '高等数学', '水电费', 1, 1, 1, 1, 1, '2022-01-22', '2022-01-29', 44, 10, 10, '2022-01-18 13:11:50', '2022-01-22 19:59:48');
INSERT INTO `el_exam` VALUES ('24a64a92-150e-49ce-b376-e2513f28192e', '高等数学2', '22', 1, 1, 1, 0, NULL, NULL, NULL, 60, 45, 20, '2022-01-19 12:28:16', '2022-01-22 20:04:55');
INSERT INTO `el_exam` VALUES ('3196810c-4a9d-4e2c-9fad-f740868c5024', '电路原理', '哈哈哈', 2, 1, 1, 1, 1, '2022-01-18', '2022-01-25', 44, 45, 0, '2022-01-18 12:40:25', '2022-01-23 16:49:16');
INSERT INTO `el_exam` VALUES ('36b481e0-658f-4406-b1e1-7dc5fe83ee3d', '体育考试', '11', 2, 1, 0, 2, 1, '2022-01-11', '2022-01-27', 24, 12, 12, '2022-01-18 23:21:46', '2022-01-22 21:34:24');
INSERT INTO `el_exam` VALUES ('4fed3060-be60-4932-b01a-9849a36c6999', '软件设计', '设计课程', 1, 1, 1, 0, 0, NULL, NULL, 11, 12, 5, '2022-01-18 22:55:01', '2022-01-22 21:06:56');
INSERT INTO `el_exam` VALUES ('622d3bd1-c983-4e03-b31a-5aedee3b8615', '财务知识考试', '11', 2, 1, 1, 1, 1, '2022-01-04', '2022-01-06', 44, 1, 12, '2022-01-18 22:34:18', '2022-01-22 21:43:51');
INSERT INTO `el_exam` VALUES ('8db66680-1516-4383-8261-d972fedbc806', '1111111111', '22222222', 2, 1, NULL, 2, 1, NULL, NULL, 33, 22, 22, '2022-01-18 13:00:39', '2022-01-18 13:00:39');
INSERT INTO `el_exam` VALUES ('8ddb9807-6e43-4e2b-997e-87ba12a51390', '1号', '是', 2, 1, 0, 3, 1, '2022-01-19', '2022-02-20', 12, 12, 1, '2022-01-19 22:22:35', '2022-01-19 22:22:35');
INSERT INTO `el_exam` VALUES ('99b4e1d3-cf04-4772-b1a9-a032c1443aa4', '0000', '00', 2, 1, NULL, 3, 1, '2022-01-18', '2022-02-20', 33, 10, 10, '2022-01-18 13:14:33', '2022-01-18 13:14:33');
INSERT INTO `el_exam` VALUES ('a05ff2f6-7dc8-4016-b42f-da46ee738533', '121212', '12', 2, 1, NULL, 1, 0, NULL, NULL, 11, 12, 1, '2022-01-18 23:07:47', '2022-01-18 23:07:47');
INSERT INTO `el_exam` VALUES ('a3da4e6f-6c46-4f94-b42d-8af01bc7d0ad', '2111111111111', '11', 1, 1, NULL, 2, 0, NULL, NULL, 11, 1, 1, '2022-01-18 23:10:52', '2022-01-18 23:10:52');
INSERT INTO `el_exam` VALUES ('bc6c3d96-4480-4fb0-9d45-64d8a956c30b', '哈哈哈哈', '2223', 2, 1, 1, 0, 1, '2022-01-11', '2022-01-26', 44, 12, 0, '2022-01-18 23:02:03', '2022-01-31 20:49:30');
INSERT INTO `el_exam` VALUES ('d8d584ec-cd66-4805-9ec8-eb215ab44d7c', '测试44', '22', 2, 1, NULL, 0, 0, NULL, NULL, 66, 12, 22, '2022-01-18 22:44:47', '2022-01-18 22:44:47');
INSERT INTO `el_exam` VALUES ('dfa9f177-eca3-4001-b0d8-92c27f01062f', '测试3', '嘟嘟嘟嘟', 2, 1, NULL, 1, 1, NULL, NULL, 66, 40, 50, '2022-01-18 22:31:40', '2022-01-18 22:31:40');
INSERT INTO `el_exam` VALUES ('f6dbef3d-de71-47a9-8b46-c44387aff1be', '1212', '2121', 2, 1, NULL, 2, 1, '2022-01-04', '2022-02-28', 152, 12, 152, '2022-01-18 23:24:32', '2022-01-18 23:24:32');

-- ----------------------------
-- Table structure for el_exam_depart
-- ----------------------------
DROP TABLE IF EXISTS `el_exam_depart`;
CREATE TABLE `el_exam_depart`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `depart_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `el_exam_depart_depart_id_exam_id_unique`(`exam_id`, `depart_id`) USING BTREE,
  INDEX `depart_id`(`depart_id`) USING BTREE,
  CONSTRAINT `el_exam_depart_ibfk_13` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `el_exam_depart_ibfk_14` FOREIGN KEY (`depart_id`) REFERENCES `sys_depart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam_depart
-- ----------------------------
INSERT INTO `el_exam_depart` VALUES (1, '2022-01-22 21:34:24', '2022-01-22 21:34:24', '36b481e0-658f-4406-b1e1-7dc5fe83ee3d', 3);
INSERT INTO `el_exam_depart` VALUES (2, '2022-01-22 21:34:24', '2022-01-22 21:34:24', '36b481e0-658f-4406-b1e1-7dc5fe83ee3d', 4);
INSERT INTO `el_exam_depart` VALUES (3, '2022-01-22 21:34:24', '2022-01-22 21:34:24', '36b481e0-658f-4406-b1e1-7dc5fe83ee3d', 7);
INSERT INTO `el_exam_depart` VALUES (4, '2022-01-22 21:34:24', '2022-01-22 21:34:24', '36b481e0-658f-4406-b1e1-7dc5fe83ee3d', 8);
INSERT INTO `el_exam_depart` VALUES (5, '2022-01-22 21:43:51', '2022-01-22 21:43:51', '622d3bd1-c983-4e03-b31a-5aedee3b8615', 4);
INSERT INTO `el_exam_depart` VALUES (6, '2022-01-22 21:43:51', '2022-01-22 21:43:51', '622d3bd1-c983-4e03-b31a-5aedee3b8615', 5);
INSERT INTO `el_exam_depart` VALUES (7, '2022-01-23 16:49:16', '2022-01-23 16:49:16', '3196810c-4a9d-4e2c-9fad-f740868c5024', 3);
INSERT INTO `el_exam_depart` VALUES (8, '2022-01-23 16:49:16', '2022-01-23 16:49:16', '3196810c-4a9d-4e2c-9fad-f740868c5024', 4);
INSERT INTO `el_exam_depart` VALUES (9, '2022-01-31 20:49:30', '2022-01-31 20:49:30', 'bc6c3d96-4480-4fb0-9d45-64d8a956c30b', 3);
INSERT INTO `el_exam_depart` VALUES (10, '2022-01-31 20:49:30', '2022-01-31 20:49:30', 'bc6c3d96-4480-4fb0-9d45-64d8a956c30b', 4);

-- ----------------------------
-- Table structure for el_exam_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_exam_repo`;
CREATE TABLE `el_exam_repo`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `radio_count` int(0) NULL DEFAULT 0,
  `radio_score` int(0) NULL DEFAULT 0,
  `multi_count` int(0) NULL DEFAULT 0,
  `multi_score` int(0) NULL DEFAULT 0,
  `judge_count` int(0) NULL DEFAULT 0,
  `judge_score` int(0) NULL DEFAULT 0,
  `saq_count` int(0) NULL DEFAULT 0,
  `saq_score` int(0) NULL DEFAULT 0,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `repo_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  INDEX `repo_id`(`repo_id`) USING BTREE,
  CONSTRAINT `el_exam_repo_ibfk_67` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_exam_repo_ibfk_68` FOREIGN KEY (`repo_id`) REFERENCES `el_repo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam_repo
-- ----------------------------
INSERT INTO `el_exam_repo` VALUES ('204b76a3-52ed-4404-b4f9-3416a26afdad', 30, 2, 0, 0, 0, 0, 0, NULL, '2022-01-19 12:28:16', '2022-01-22 20:04:55', '24a64a92-150e-49ce-b376-e2513f28192e', 2);
INSERT INTO `el_exam_repo` VALUES ('45171fe4-2f5b-44ac-a969-f35bfd3b81c6', 22, 2, 0, 0, 0, 0, 0, NULL, '2022-01-23 16:49:16', '2022-01-23 16:49:16', '3196810c-4a9d-4e2c-9fad-f740868c5024', 2);
INSERT INTO `el_exam_repo` VALUES ('4df13eaf-8690-4d57-b7d6-eabc22629e09', 12, 1, 0, 0, 0, 0, 0, NULL, '2022-01-19 22:22:35', '2022-01-19 22:22:35', '8ddb9807-6e43-4e2b-997e-87ba12a51390', 2);
INSERT INTO `el_exam_repo` VALUES ('640d65b8-de25-451b-abc5-33261434dfd4', 22, 2, 0, 0, 0, 0, 0, NULL, '2022-01-31 20:49:30', '2022-01-31 20:49:30', 'bc6c3d96-4480-4fb0-9d45-64d8a956c30b', 2);
INSERT INTO `el_exam_repo` VALUES ('71612b28-87c6-43c0-ae07-cbe4b3952a6e', 12, 2, 0, 0, 0, 0, 0, NULL, '2022-01-18 23:21:46', '2022-01-22 21:34:24', '36b481e0-658f-4406-b1e1-7dc5fe83ee3d', 2);
INSERT INTO `el_exam_repo` VALUES ('87c10b30-77d3-4648-863a-abf477fddfdc', 11, 1, 0, 0, 0, 0, 0, NULL, '2022-01-22 21:06:56', '2022-01-22 21:06:56', '4fed3060-be60-4932-b01a-9849a36c6999', 2);
INSERT INTO `el_exam_repo` VALUES ('9a1b9747-7831-40fb-a6bb-dfa77d9f05a3', 22, 2, 0, 0, 0, 0, 0, NULL, '2022-01-22 21:43:51', '2022-01-22 21:43:51', '622d3bd1-c983-4e03-b31a-5aedee3b8615', 2);
INSERT INTO `el_exam_repo` VALUES ('bd984f72-c8d3-4b13-903e-8b2498ac9837', 22, 2, 0, 0, 0, 0, 0, NULL, '2022-01-20 13:12:23', '2022-01-22 19:59:48', '1bcc2706-f734-4fd5-9c99-80971cf97c05', 2);
INSERT INTO `el_exam_repo` VALUES ('d0b6edbc-90af-482f-9c55-cfe7cf15f693', 12, 1, 0, 0, 0, 0, 0, NULL, '2022-01-20 13:10:36', '2022-01-22 21:42:53', '1a0f77f0-3f4c-406f-b892-d679735829f1', 2);
INSERT INTO `el_exam_repo` VALUES ('d7f8a698-9ed0-49c2-87cb-5b585cb79bbf', 12, 11, 20, 1, 0, 0, 0, NULL, '2022-01-18 23:24:32', '2022-01-18 23:24:32', 'f6dbef3d-de71-47a9-8b46-c44387aff1be', 2);
INSERT INTO `el_exam_repo` VALUES ('ed5840c7-f4c7-40c6-ad95-b33128e7c2ea', 11, 1, 0, 0, 0, 0, 0, NULL, '2022-01-20 12:46:04', '2022-01-22 21:42:31', '0aff40fb-2147-4364-983a-41013dc6aee8', 2);

-- ----------------------------
-- Table structure for el_paper
-- ----------------------------
DROP TABLE IF EXISTS `el_paper`;
CREATE TABLE `el_paper`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `total_time` int(0) NULL DEFAULT NULL,
  `user_time` int(0) NULL DEFAULT NULL,
  `total_score` int(0) NULL DEFAULT NULL,
  `qualify_score` int(0) NULL DEFAULT NULL,
  `subj_score` int(0) NULL DEFAULT NULL,
  `has_saq` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `limit_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `depart_id` int(0) NULL DEFAULT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `user_score` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `depart_id`(`depart_id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  CONSTRAINT `el_paper_ibfk_100` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_ibfk_101` FOREIGN KEY (`depart_id`) REFERENCES `sys_depart` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_ibfk_102` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper
-- ----------------------------
INSERT INTO `el_paper` VALUES (36, '张三的第一次考试', 50, 35, 100, 80, NULL, NULL, NULL, NULL, '2022-02-06 10:50:49', '2022-02-06 10:50:49', 1, NULL, '0aff40fb-2147-4364-983a-41013dc6aee8', 62);
INSERT INTO `el_paper` VALUES (39, '大学物理', 50, NULL, 11, 10, NULL, NULL, NULL, NULL, '2022-02-06 19:04:57', '2022-02-06 19:04:57', 1, 2, '0aff40fb-2147-4364-983a-41013dc6aee8', NULL);
INSERT INTO `el_paper` VALUES (40, '大学物理', 50, NULL, 11, 10, NULL, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:23:29', 1, 2, '0aff40fb-2147-4364-983a-41013dc6aee8', NULL);
INSERT INTO `el_paper` VALUES (41, '大学物理', 50, NULL, 11, 10, NULL, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:27', 1, 2, '0aff40fb-2147-4364-983a-41013dc6aee8', NULL);

-- ----------------------------
-- Table structure for el_paper_qu
-- ----------------------------
DROP TABLE IF EXISTS `el_paper_qu`;
CREATE TABLE `el_paper_qu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL,
  `answered` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `score` int(0) NULL DEFAULT NULL,
  `actual_sorce` int(0) NULL DEFAULT NULL,
  `is_right` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `paper_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `paper_id`(`paper_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_paper_qu_ibfk_67` FOREIGN KEY (`paper_id`) REFERENCES `el_paper` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_ibfk_68` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper_qu
-- ----------------------------
INSERT INTO `el_paper_qu` VALUES (63, 1, 1, 0, NULL, NULL, NULL, '2022-02-06 10:50:49', '2022-02-06 15:13:29', 36, 1);
INSERT INTO `el_paper_qu` VALUES (64, 1, 1, 1, NULL, NULL, NULL, '2022-02-06 10:50:49', '2022-02-06 15:06:47', 36, 4);
INSERT INTO `el_paper_qu` VALUES (65, 3, 1, 3, NULL, NULL, NULL, '2022-02-06 10:50:49', '2022-02-06 11:06:40', 36, 8);
INSERT INTO `el_paper_qu` VALUES (66, 2, 1, 2, NULL, NULL, NULL, '2022-02-06 10:50:49', '2022-02-06 15:06:48', 36, 16);
INSERT INTO `el_paper_qu` VALUES (75, 1, 1, 0, NULL, NULL, NULL, '2022-02-06 19:04:57', '2022-02-06 19:05:26', 39, 4);
INSERT INTO `el_paper_qu` VALUES (76, 1, 1, 1, NULL, NULL, NULL, '2022-02-06 19:04:57', '2022-02-06 19:05:28', 39, 1);
INSERT INTO `el_paper_qu` VALUES (77, 3, 1, 3, NULL, NULL, NULL, '2022-02-06 19:04:57', '2022-02-06 19:05:40', 39, 8);
INSERT INTO `el_paper_qu` VALUES (78, 2, 1, 2, NULL, NULL, NULL, '2022-02-06 19:04:57', '2022-02-06 19:05:31', 39, 16);
INSERT INTO `el_paper_qu` VALUES (79, 1, 1, 0, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:21', 40, 1);
INSERT INTO `el_paper_qu` VALUES (80, 1, 1, 1, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:18', 40, 11);
INSERT INTO `el_paper_qu` VALUES (81, 1, 1, 2, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:16', 40, 12);
INSERT INTO `el_paper_qu` VALUES (82, 1, 1, 3, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:15', 40, 14);
INSERT INTO `el_paper_qu` VALUES (83, 1, 1, 4, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:13', 40, 15);
INSERT INTO `el_paper_qu` VALUES (84, 3, 1, 6, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:07', 40, 10);
INSERT INTO `el_paper_qu` VALUES (85, 3, 1, 7, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:06', 40, 13);
INSERT INTO `el_paper_qu` VALUES (86, 2, 1, 5, NULL, NULL, NULL, '2022-02-06 22:23:29', '2022-02-06 22:31:10', 40, 16);
INSERT INTO `el_paper_qu` VALUES (87, 1, 1, 0, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:33', 41, 1);
INSERT INTO `el_paper_qu` VALUES (88, 1, 1, 1, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:35', 41, 11);
INSERT INTO `el_paper_qu` VALUES (89, 1, 1, 2, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:36', 41, 12);
INSERT INTO `el_paper_qu` VALUES (90, 1, 1, 3, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:38', 41, 14);
INSERT INTO `el_paper_qu` VALUES (91, 1, 1, 4, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:40', 41, 15);
INSERT INTO `el_paper_qu` VALUES (92, 3, 1, 6, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:50', 41, 10);
INSERT INTO `el_paper_qu` VALUES (93, 3, 1, 7, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:47', 41, 13);
INSERT INTO `el_paper_qu` VALUES (94, 2, 1, 5, NULL, NULL, NULL, '2022-02-07 12:27:27', '2022-02-07 12:27:43', 41, 16);

-- ----------------------------
-- Table structure for el_paper_qu_answer
-- ----------------------------
DROP TABLE IF EXISTS `el_paper_qu_answer`;
CREATE TABLE `el_paper_qu_answer`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `is_right` int(0) NULL DEFAULT NULL,
  `checked` int(0) NULL DEFAULT 0,
  `sort` int(0) NULL DEFAULT NULL,
  `abc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `paper_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  `answer_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `paper_id`(`paper_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  INDEX `answer_id`(`answer_id`) USING BTREE,
  CONSTRAINT `el_paper_qu_answer_ibfk_100` FOREIGN KEY (`answer_id`) REFERENCES `el_qu_answer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_answer_ibfk_98` FOREIGN KEY (`paper_id`) REFERENCES `el_paper` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_answer_ibfk_99` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 219 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper_qu_answer
-- ----------------------------
INSERT INTO `el_paper_qu_answer` VALUES (138, 1, 1, NULL, 'A', '2022-02-06 10:50:49', '2022-02-06 15:13:29', 36, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (139, 0, 0, NULL, 'B', '2022-02-06 10:50:49', '2022-02-06 15:13:29', 36, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (140, 0, 0, NULL, 'C', '2022-02-06 10:50:49', '2022-02-06 15:13:29', 36, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (141, 0, 0, NULL, 'D', '2022-02-06 10:50:49', '2022-02-06 15:13:29', 36, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (142, 1, 0, NULL, 'A', '2022-02-06 10:51:22', '2022-02-06 15:06:47', 36, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (143, 0, 1, NULL, 'B', '2022-02-06 10:51:22', '2022-02-06 15:06:47', 36, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (144, 0, 0, NULL, 'C', '2022-02-06 10:51:22', '2022-02-06 15:06:47', 36, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (145, 0, 0, NULL, 'D', '2022-02-06 10:51:22', '2022-02-06 15:06:47', 36, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (146, 1, 0, NULL, 'A', '2022-02-06 10:51:25', '2022-02-06 15:06:48', 36, 16, 66);
INSERT INTO `el_paper_qu_answer` VALUES (147, 0, 1, NULL, 'B', '2022-02-06 10:51:25', '2022-02-06 15:06:48', 36, 16, 67);
INSERT INTO `el_paper_qu_answer` VALUES (148, 1, 1, NULL, 'C', '2022-02-06 10:51:25', '2022-02-06 15:06:48', 36, 16, 68);
INSERT INTO `el_paper_qu_answer` VALUES (149, 0, 0, NULL, 'D', '2022-02-06 10:51:25', '2022-02-06 15:06:48', 36, 16, 69);
INSERT INTO `el_paper_qu_answer` VALUES (150, 1, 1, NULL, 'A', '2022-02-06 10:53:04', '2022-02-06 11:06:40', 36, 8, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (168, 1, 0, NULL, 'A', '2022-02-06 19:04:57', '2022-02-06 19:05:26', 39, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (169, 0, 1, NULL, 'B', '2022-02-06 19:04:57', '2022-02-06 19:05:26', 39, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (170, 0, 0, NULL, 'C', '2022-02-06 19:04:58', '2022-02-06 19:05:26', 39, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (171, 0, 0, NULL, 'D', '2022-02-06 19:04:58', '2022-02-06 19:05:26', 39, 4, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (172, 1, 0, NULL, 'A', '2022-02-06 19:05:26', '2022-02-06 19:05:28', 39, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (173, 0, 1, NULL, 'B', '2022-02-06 19:05:26', '2022-02-06 19:05:28', 39, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (174, 0, 0, NULL, 'C', '2022-02-06 19:05:26', '2022-02-06 19:05:28', 39, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (175, 0, 0, NULL, 'D', '2022-02-06 19:05:26', '2022-02-06 19:05:28', 39, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (176, 1, 0, NULL, 'A', '2022-02-06 19:05:28', '2022-02-06 19:05:31', 39, 16, 66);
INSERT INTO `el_paper_qu_answer` VALUES (177, 0, 0, NULL, 'B', '2022-02-06 19:05:28', '2022-02-06 19:05:31', 39, 16, 67);
INSERT INTO `el_paper_qu_answer` VALUES (178, 1, 1, NULL, 'C', '2022-02-06 19:05:28', '2022-02-06 19:05:31', 39, 16, 68);
INSERT INTO `el_paper_qu_answer` VALUES (179, 0, 1, NULL, 'D', '2022-02-06 19:05:28', '2022-02-06 19:05:31', 39, 16, 69);
INSERT INTO `el_paper_qu_answer` VALUES (180, 1, 1, NULL, 'A', '2022-02-06 19:05:31', '2022-02-06 19:05:40', 39, 8, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (181, 1, 0, NULL, 'A', '2022-02-06 22:30:35', '2022-02-06 22:31:21', 40, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (182, 0, 1, NULL, 'B', '2022-02-06 22:30:35', '2022-02-06 22:31:21', 40, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (183, 0, 0, NULL, 'C', '2022-02-06 22:30:35', '2022-02-06 22:31:21', 40, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (184, 0, 0, NULL, 'D', '2022-02-06 22:30:35', '2022-02-06 22:31:21', 40, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (185, 0, 0, NULL, 'A', '2022-02-06 22:30:55', '2022-02-06 22:31:15', 40, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (186, 1, 1, NULL, 'B', '2022-02-06 22:30:55', '2022-02-06 22:31:15', 40, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (187, 0, 0, NULL, 'C', '2022-02-06 22:30:55', '2022-02-06 22:31:15', 40, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (188, 1, 0, NULL, 'A', '2022-02-06 22:30:57', '2022-02-06 22:31:13', 40, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (189, 0, 0, NULL, 'B', '2022-02-06 22:30:57', '2022-02-06 22:31:13', 40, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (190, 0, 1, NULL, 'C', '2022-02-06 22:30:57', '2022-02-06 22:31:13', 40, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (191, 0, 0, NULL, 'D', '2022-02-06 22:30:57', '2022-02-06 22:31:13', 40, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (192, 1, 0, NULL, 'A', '2022-02-06 22:30:59', '2022-02-06 22:31:10', 40, 16, 66);
INSERT INTO `el_paper_qu_answer` VALUES (193, 0, 1, NULL, 'B', '2022-02-06 22:30:59', '2022-02-06 22:31:10', 40, 16, 67);
INSERT INTO `el_paper_qu_answer` VALUES (194, 1, 1, NULL, 'C', '2022-02-06 22:30:59', '2022-02-06 22:31:10', 40, 16, 68);
INSERT INTO `el_paper_qu_answer` VALUES (195, 0, 0, NULL, 'D', '2022-02-06 22:30:59', '2022-02-06 22:31:10', 40, 16, 69);
INSERT INTO `el_paper_qu_answer` VALUES (196, 1, 0, NULL, 'A', '2022-02-06 22:31:00', '2022-02-06 22:31:07', 40, 10, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (197, 0, 1, NULL, 'B', '2022-02-06 22:31:00', '2022-02-06 22:31:07', 40, 10, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (198, 1, 0, NULL, 'A', '2022-02-06 22:31:02', '2022-02-06 22:31:06', 40, 13, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (199, 0, 1, NULL, 'B', '2022-02-06 22:31:02', '2022-02-06 22:31:06', 40, 13, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (200, 1, 1, NULL, 'A', '2022-02-07 12:27:28', '2022-02-07 12:27:33', 41, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (201, 0, 0, NULL, 'B', '2022-02-07 12:27:28', '2022-02-07 12:27:33', 41, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (202, 0, 0, NULL, 'C', '2022-02-07 12:27:28', '2022-02-07 12:27:33', 41, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (203, 0, 0, NULL, 'D', '2022-02-07 12:27:28', '2022-02-07 12:27:33', 41, 1, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (204, 0, 0, NULL, 'A', '2022-02-07 12:27:36', '2022-02-07 12:27:38', 41, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (205, 1, 1, NULL, 'B', '2022-02-07 12:27:36', '2022-02-07 12:27:38', 41, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (206, 0, 0, NULL, 'C', '2022-02-07 12:27:36', '2022-02-07 12:27:38', 41, 14, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (207, 1, 0, NULL, 'A', '2022-02-07 12:27:38', '2022-02-07 12:27:40', 41, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (208, 0, 0, NULL, 'B', '2022-02-07 12:27:38', '2022-02-07 12:27:40', 41, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (209, 0, 1, NULL, 'C', '2022-02-07 12:27:38', '2022-02-07 12:27:40', 41, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (210, 0, 0, NULL, 'D', '2022-02-07 12:27:38', '2022-02-07 12:27:40', 41, 15, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (211, 1, 0, NULL, 'A', '2022-02-07 12:27:40', '2022-02-07 12:27:43', 41, 16, 66);
INSERT INTO `el_paper_qu_answer` VALUES (212, 0, 1, NULL, 'B', '2022-02-07 12:27:40', '2022-02-07 12:27:43', 41, 16, 67);
INSERT INTO `el_paper_qu_answer` VALUES (213, 1, 1, NULL, 'C', '2022-02-07 12:27:40', '2022-02-07 12:27:43', 41, 16, 68);
INSERT INTO `el_paper_qu_answer` VALUES (214, 0, 0, NULL, 'D', '2022-02-07 12:27:40', '2022-02-07 12:27:43', 41, 16, 69);
INSERT INTO `el_paper_qu_answer` VALUES (215, 1, 0, NULL, 'A', '2022-02-07 12:27:43', '2022-02-07 12:27:50', 41, 10, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (216, 0, 1, NULL, 'B', '2022-02-07 12:27:43', '2022-02-07 12:27:50', 41, 10, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (217, 1, 1, NULL, 'A', '2022-02-07 12:27:45', '2022-02-07 12:27:47', 41, 13, NULL);
INSERT INTO `el_paper_qu_answer` VALUES (218, 0, 0, NULL, 'B', '2022-02-07 12:27:45', '2022-02-07 12:27:47', 41, 13, NULL);

-- ----------------------------
-- Table structure for el_qu
-- ----------------------------
DROP TABLE IF EXISTS `el_qu`;
CREATE TABLE `el_qu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL COMMENT '题目类型，1单选，2多选，3判断',
  `level` int(0) NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `analysis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu
-- ----------------------------
INSERT INTO `el_qu` VALUES (1, 1, 1, NULL, '全军各级要坚决贯彻党中央和中央军委决策指示，密切关注国家安全和军事斗争形势变化，紧盯科技之变、战争之变、对手之变，大力团', NULL, '更丰富付付付付付付付', '2022-01-16 18:29:01', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (4, 1, 1, NULL, '湛江属于哪个省？', NULL, '', '2022-01-16 18:45:40', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (8, 3, 1, NULL, '身无彩凤双飞翼，下一句是：比翼双飞哈哈哈', NULL, '诗词', '2022-01-16 18:54:29', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (10, 3, 1, NULL, '九天神帝决的作者是天蚕土豆老师？', NULL, '不对', '2022-01-16 19:13:57', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (11, 1, 1, NULL, '东方日出西方日落，嘟嘟嘟嘟嘟', NULL, '哒哒哒哒哒哒多', '2022-01-16 19:16:32', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (12, 1, 1, NULL, '滚滚长江东逝水', NULL, '热热热热热若', '2022-01-16 19:19:59', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (13, 3, 1, NULL, '一江春水向东流', NULL, '333333', '2022-01-16 19:22:44', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (14, 1, 2, NULL, '哈哈哈哈成功了', NULL, '', '2022-01-16 19:49:55', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (15, 1, 2, NULL, '山无论，天地合，才敢与君决。', NULL, '', '2022-01-16 19:50:56', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (16, 2, 1, NULL, '模型是 Sequelize 的本质. 模型是代表数据库中表的抽象. 在 Sequelize 中,它是一个 Model 的扩展类.\n\n该模型告诉 Sequelize 有关它代表的实体的几件事,例如数据库中表的名称以及它具有的列(及其数据类型).\n\nSequelize 中的模型有一个名称. 此名称不必与它在数据库中表示的表的名称相同. 通常,模型具有单数名称(例如,User),而表具有复数名称(例如, Users),当然这是完全可配置的.', NULL, '模型是 Sequelize 的本质. 模型是代表数据库中表的抽象. 在 Sequelize 中,它是一个 Model 的扩展类.\n\n该模型告诉 Sequelize 有关它代表的实体的几件事,例如数据库中表的名称以及它具有的列(及其数据类型).\n\nSequelize 中的模型有一个名称. 此名称不必与它在数据库中表示的表的名称相同. 通常,模型具有单数名称(例如,User),而表具有复数名称(例如, Users),当然这是完全可配置的.', '2022-01-17 21:12:48', '2022-02-09 21:46:33', 0);
INSERT INTO `el_qu` VALUES (18, 1, 1, NULL, '窗前明月光', NULL, NULL, '2022-02-09 21:43:40', '2022-02-09 21:43:40', 0);

-- ----------------------------
-- Table structure for el_qu_answer
-- ----------------------------
DROP TABLE IF EXISTS `el_qu_answer`;
CREATE TABLE `el_qu_answer`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '题目选项编号，自动递增',
  `is_right` int(0) NULL DEFAULT NULL COMMENT '选项是否为正确答案，0错，1对',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '选项涉及的图片路径',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '选项',
  `analysis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '解析',
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `qu_id` int(0) NULL DEFAULT NULL COMMENT '选项关联的题目id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_qu_answer_ibfk_1` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 85 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu_answer
-- ----------------------------
INSERT INTO `el_qu_answer` VALUES (66, 1, NULL, '胜多负少', '', '2022-01-17 21:12:48', '2022-01-17 21:12:48', 16);
INSERT INTO `el_qu_answer` VALUES (67, 0, NULL, '嘟嘟嘟嘟的', '', '2022-01-17 21:12:48', '2022-01-17 21:12:48', 16);
INSERT INTO `el_qu_answer` VALUES (68, 1, NULL, '哒哒哒哒哒哒多', '', '2022-01-17 21:12:48', '2022-01-17 21:12:48', 16);
INSERT INTO `el_qu_answer` VALUES (69, 0, NULL, '哒哒哒哒哒哒多', '', '2022-01-17 21:12:48', '2022-01-17 21:12:48', 16);
INSERT INTO `el_qu_answer` VALUES (83, 1, NULL, '正确', NULL, '2022-02-07 12:26:41', '2022-02-07 12:26:41', 8);
INSERT INTO `el_qu_answer` VALUES (84, 0, NULL, '错误', '', '2022-02-07 12:26:41', '2022-02-07 12:26:41', 8);
INSERT INTO `el_qu_answer` VALUES (89, 0, NULL, '1', '', '2022-02-09 12:25:11', '2022-02-09 12:25:11', 12);
INSERT INTO `el_qu_answer` VALUES (90, 0, NULL, '2', '', '2022-02-09 12:25:11', '2022-02-09 12:25:11', 12);
INSERT INTO `el_qu_answer` VALUES (91, 1, NULL, '3', '', '2022-02-09 12:25:11', '2022-02-09 12:25:11', 12);
INSERT INTO `el_qu_answer` VALUES (92, 0, NULL, '4', '', '2022-02-09 12:25:11', '2022-02-09 12:25:11', 12);
INSERT INTO `el_qu_answer` VALUES (93, 1, NULL, '正确', '', '2022-02-09 12:25:25', '2022-02-09 12:25:25', 13);
INSERT INTO `el_qu_answer` VALUES (94, 0, NULL, '错误', '', '2022-02-09 12:25:25', '2022-02-09 12:25:25', 13);
INSERT INTO `el_qu_answer` VALUES (99, 1, NULL, '正确', '', '2022-02-09 20:07:06', '2022-02-09 20:07:06', 10);
INSERT INTO `el_qu_answer` VALUES (100, 0, NULL, '错误', '', '2022-02-09 20:07:06', '2022-02-09 20:07:06', 10);
INSERT INTO `el_qu_answer` VALUES (101, 0, NULL, '阿斯蒂芬', '搜索', '2022-02-09 20:08:59', '2022-02-09 20:08:59', 14);
INSERT INTO `el_qu_answer` VALUES (102, 1, NULL, '阿斯蒂芬', '水电费', '2022-02-09 20:08:59', '2022-02-09 20:08:59', 14);
INSERT INTO `el_qu_answer` VALUES (103, 0, NULL, '水电费', '', '2022-02-09 20:08:59', '2022-02-09 20:08:59', 14);
INSERT INTO `el_qu_answer` VALUES (148, 1, NULL, '正确', '', '2022-02-09 21:14:58', '2022-02-09 21:14:58', 1);
INSERT INTO `el_qu_answer` VALUES (149, 0, NULL, '错误', '', '2022-02-09 21:14:58', '2022-02-09 21:14:58', 1);
INSERT INTO `el_qu_answer` VALUES (150, 0, NULL, '待定', '', '2022-02-09 21:14:58', '2022-02-09 21:14:58', 1);
INSERT INTO `el_qu_answer` VALUES (151, 0, NULL, '不忘初心', '', '2022-02-09 21:14:58', '2022-02-09 21:14:58', 1);
INSERT INTO `el_qu_answer` VALUES (152, 1, NULL, '黄忠', '', '2022-02-09 21:22:32', '2022-02-09 21:22:32', 15);
INSERT INTO `el_qu_answer` VALUES (153, 0, NULL, '孙策', '', '2022-02-09 21:22:32', '2022-02-09 21:22:32', 15);
INSERT INTO `el_qu_answer` VALUES (154, 0, NULL, '后裔', '', '2022-02-09 21:22:32', '2022-02-09 21:22:32', 15);
INSERT INTO `el_qu_answer` VALUES (155, 0, NULL, '甄姬', '是否', '2022-02-09 21:22:32', '2022-02-09 21:22:32', 15);
INSERT INTO `el_qu_answer` VALUES (156, 1, NULL, '多来嗯梦', '', '2022-02-09 21:25:25', '2022-02-09 21:25:25', 11);
INSERT INTO `el_qu_answer` VALUES (157, 0, NULL, '哈利波特', '', '2022-02-09 21:25:25', '2022-02-09 21:25:25', 11);
INSERT INTO `el_qu_answer` VALUES (158, 0, NULL, '宙斯', '', '2022-02-09 21:25:25', '2022-02-09 21:25:25', 11);
INSERT INTO `el_qu_answer` VALUES (159, 0, NULL, '维也纳', '', '2022-02-09 21:25:25', '2022-02-09 21:25:25', 11);
INSERT INTO `el_qu_answer` VALUES (160, 1, NULL, '广东省', '', '2022-02-09 21:31:09', '2022-02-09 21:31:09', 4);
INSERT INTO `el_qu_answer` VALUES (161, 0, NULL, '海南省', '', '2022-02-09 21:31:09', '2022-02-09 21:31:09', 4);
INSERT INTO `el_qu_answer` VALUES (162, 0, NULL, '广西省', '', '2022-02-09 21:31:09', '2022-02-09 21:31:09', 4);
INSERT INTO `el_qu_answer` VALUES (163, 0, NULL, '湖北省', '', '2022-02-09 21:31:09', '2022-02-09 21:31:09', 4);
INSERT INTO `el_qu_answer` VALUES (164, 1, NULL, '疑是地上霜', '', '2022-02-09 21:43:41', '2022-02-09 21:43:41', 18);
INSERT INTO `el_qu_answer` VALUES (165, 0, NULL, '举头望明月', '', '2022-02-09 21:43:41', '2022-02-09 21:43:41', 18);
INSERT INTO `el_qu_answer` VALUES (166, 0, NULL, '低头思故乡', '', '2022-02-09 21:43:41', '2022-02-09 21:43:41', 18);
INSERT INTO `el_qu_answer` VALUES (167, 0, NULL, '三尺神明', '', '2022-02-09 21:43:41', '2022-02-09 21:43:41', 18);

-- ----------------------------
-- Table structure for el_qu_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_qu_repo`;
CREATE TABLE `el_qu_repo`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  `repo_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  INDEX `repo_id`(`repo_id`) USING BTREE,
  CONSTRAINT `el_qu_repo_ibfk_67` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `el_qu_repo_ibfk_68` FOREIGN KEY (`repo_id`) REFERENCES `el_repo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu_repo
-- ----------------------------
INSERT INTO `el_qu_repo` VALUES (70, 2, NULL, 16, 6);
INSERT INTO `el_qu_repo` VALUES (80, 2, NULL, 16, 9);
INSERT INTO `el_qu_repo` VALUES (90, 2, NULL, 16, 2);
INSERT INTO `el_qu_repo` VALUES (93, NULL, NULL, 8, 6);
INSERT INTO `el_qu_repo` VALUES (94, NULL, NULL, 8, 9);
INSERT INTO `el_qu_repo` VALUES (95, NULL, NULL, 8, 8);
INSERT INTO `el_qu_repo` VALUES (99, NULL, NULL, 12, 6);
INSERT INTO `el_qu_repo` VALUES (100, NULL, NULL, 12, 9);
INSERT INTO `el_qu_repo` VALUES (101, NULL, NULL, 12, 8);
INSERT INTO `el_qu_repo` VALUES (102, NULL, NULL, 12, 2);
INSERT INTO `el_qu_repo` VALUES (103, NULL, NULL, 13, 6);
INSERT INTO `el_qu_repo` VALUES (104, NULL, NULL, 13, 9);
INSERT INTO `el_qu_repo` VALUES (105, NULL, NULL, 13, 2);
INSERT INTO `el_qu_repo` VALUES (111, NULL, NULL, 10, 6);
INSERT INTO `el_qu_repo` VALUES (112, NULL, NULL, 10, 2);
INSERT INTO `el_qu_repo` VALUES (113, NULL, NULL, 14, 6);
INSERT INTO `el_qu_repo` VALUES (114, NULL, NULL, 14, 9);
INSERT INTO `el_qu_repo` VALUES (115, NULL, NULL, 14, 2);
INSERT INTO `el_qu_repo` VALUES (140, 3, NULL, 10, 3);
INSERT INTO `el_qu_repo` VALUES (142, 1, NULL, 12, 3);
INSERT INTO `el_qu_repo` VALUES (146, NULL, NULL, 4, 6);
INSERT INTO `el_qu_repo` VALUES (148, NULL, NULL, 18, 2);

-- ----------------------------
-- Table structure for el_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_repo`;
CREATE TABLE `el_repo`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `multi_count` int(0) NULL DEFAULT NULL,
  `radio_count` int(0) NULL DEFAULT NULL,
  `judge_count` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_repo
-- ----------------------------
INSERT INTO `el_repo` VALUES (2, NULL, '中国词库', '词库考试大全', 0, 0, 1, '2022-01-16 11:16:29', '2022-02-09 21:22:32');
INSERT INTO `el_repo` VALUES (3, NULL, '古诗词题库大全', '涵盖所有古诗词', 1, 1, 0, '2022-01-16 11:20:53', '2022-02-09 21:31:09');
INSERT INTO `el_repo` VALUES (6, NULL, '南国诗词', '哈哈哈', 0, 0, 1, '2022-01-16 12:05:29', '2022-02-09 21:22:32');
INSERT INTO `el_repo` VALUES (8, NULL, '2022季度飞花令诗文比赛题库', '2022诗文集合', 1, 1, 0, '2022-01-16 12:05:29', '2022-02-06 20:56:10');
INSERT INTO `el_repo` VALUES (9, NULL, '北方地理', '哈哈哈哈', 0, 0, 1, '2022-01-16 12:05:29', '2022-02-09 21:22:32');

-- ----------------------------
-- Table structure for el_user_book
-- ----------------------------
DROP TABLE IF EXISTS `el_user_book`;
CREATE TABLE `el_user_book`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `wrong_count` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_user_book_ibfk_100` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_book_ibfk_101` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_book_ibfk_102` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_user_book
-- ----------------------------

-- ----------------------------
-- Table structure for el_user_exam
-- ----------------------------
DROP TABLE IF EXISTS `el_user_exam`;
CREATE TABLE `el_user_exam`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `try_count` int(0) NULL DEFAULT NULL,
  `max_score` int(0) NULL DEFAULT NULL,
  `passed` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  CONSTRAINT `el_user_exam_ibfk_13` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_exam_ibfk_14` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_user_exam
-- ----------------------------
INSERT INTO `el_user_exam` VALUES (1, 5, 88, 1, '2022-01-04 22:34:23', '2022-01-18 22:34:20', 3, '36b481e0-658f-4406-b1e1-7dc5fe83ee3d');
INSERT INTO `el_user_exam` VALUES (2, 3, 99, 0, '2022-01-22 22:35:23', '2022-01-22 22:35:21', 2, '0aff40fb-2147-4364-983a-41013dc6aee8');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `front_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `back_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `data_flag` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'ZBD考试系统', NULL, NULL, '1', '1', NULL, '2022-01-13 13:04:00', '2022-01-15 16:24:41');

-- ----------------------------
-- Table structure for sys_depart
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `depart_type` int(0) NULL DEFAULT NULL,
  `depart_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `depart_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `parent_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `sys_depart_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `sys_depart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_depart
-- ----------------------------
INSERT INTO `sys_depart` VALUES (0, NULL, '根节点', NULL, NULL, '2022-01-15 13:29:02', '2022-01-15 13:29:02', NULL);
INSERT INTO `sys_depart` VALUES (2, NULL, '盘古集团', NULL, NULL, '2022-01-15 13:32:10', '2022-01-15 16:54:15', 0);
INSERT INTO `sys_depart` VALUES (3, NULL, '研发部', NULL, NULL, '2022-01-15 13:34:09', '2022-01-15 16:56:12', 2);
INSERT INTO `sys_depart` VALUES (4, NULL, '财务部', NULL, NULL, '2022-01-15 13:35:24', '2022-01-15 16:56:07', 2);
INSERT INTO `sys_depart` VALUES (5, NULL, '后勤部', NULL, NULL, '2022-01-15 13:46:33', '2022-01-15 16:56:01', 2);
INSERT INTO `sys_depart` VALUES (6, NULL, '市场部', NULL, NULL, '2022-01-15 13:47:32', '2022-01-15 16:56:20', 2);
INSERT INTO `sys_depart` VALUES (7, NULL, '奥斯威特王国', NULL, NULL, '2022-01-15 16:55:55', '2022-01-15 16:55:55', 0);
INSERT INTO `sys_depart` VALUES (8, NULL, '甘特郡', NULL, NULL, '2022-01-15 16:58:57', '2022-01-15 16:58:57', 7);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin', '系统管理员', '2022-01-14 13:53:00', '2022-01-15 13:53:05');
INSERT INTO `sys_role` VALUES (2, 'sss', 'dfs', '2022-01-15 14:20:23', '2022-01-15 14:20:23');
INSERT INTO `sys_role` VALUES (3, 'student', '考生角色，只能参加考试，查阅个人信息。', '2022-01-15 14:21:35', '2022-01-15 14:21:35');
INSERT INTO `sys_role` VALUES (4, 'sa', '超级管理员，拥有最高权限', '2022-01-15 14:22:13', '2022-01-15 14:22:13');
INSERT INTO `sys_role` VALUES (5, 'ssd', 'ldksj', '2022-01-15 16:17:04', '2022-01-15 16:17:04');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `role_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `depart_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `data_flag` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '哈维', NULL, NULL, 'sa', '2', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 16:23:34', '2022-01-15 16:23:34');
INSERT INTO `sys_user` VALUES (2, 'admin1', '哈利波特', NULL, NULL, 'sa', '3', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 16:23:58', '2022-01-15 16:23:58');
INSERT INTO `sys_user` VALUES (3, 'student', '学徒', NULL, NULL, 'sa', '2', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 16:25:08', '2022-01-15 16:25:08');
INSERT INTO `sys_user` VALUES (4, 'student1', '门徒', NULL, NULL, 'student', '6', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 16:25:30', '2022-01-15 16:25:30');
INSERT INTO `sys_user` VALUES (5, 'student2', '都敏俊', NULL, NULL, 'student', '3', '/public/images/user_img/default.jpg', 1, NULL, '2022-01-15 16:38:31', '2022-01-15 16:49:19');
INSERT INTO `sys_user` VALUES (6, 'student3', '哈维特', NULL, NULL, 'student', '2', '/public/images/user_img/default.jpg', NULL, NULL, '2022-01-15 16:48:16', '2022-01-15 16:49:00');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `role_id` int(0) NOT NULL,
  `user_id` int(0) NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
