/*
 Navicat Premium Data Transfer

 Source Server         : sql_db
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : exam1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 15/01/2022 13:01:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for el_exam
-- ----------------------------
DROP TABLE IF EXISTS `el_exam`;
CREATE TABLE `el_exam`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `open_type` int(0) NULL DEFAULT NULL,
  `join_type` int(0) NULL DEFAULT NULL,
  `level` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `time_limit` int(0) NULL DEFAULT NULL,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `total_score` int(0) NULL DEFAULT NULL,
  `total_time` int(0) NULL DEFAULT NULL,
  `qualify_score` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam
-- ----------------------------

-- ----------------------------
-- Table structure for el_exam_depart
-- ----------------------------
DROP TABLE IF EXISTS `el_exam_depart`;
CREATE TABLE `el_exam_depart`  (
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `depart_id` int(0) NOT NULL,
  PRIMARY KEY (`exam_id`, `depart_id`) USING BTREE,
  INDEX `depart_id`(`depart_id`) USING BTREE,
  CONSTRAINT `el_exam_depart_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `el_exam_depart_ibfk_2` FOREIGN KEY (`depart_id`) REFERENCES `sys_depart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam_depart
-- ----------------------------

-- ----------------------------
-- Table structure for el_exam_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_exam_repo`;
CREATE TABLE `el_exam_repo`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `radio_count` int(0) NULL DEFAULT NULL,
  `radio_score` int(0) NULL DEFAULT NULL,
  `multi_count` int(0) NULL DEFAULT NULL,
  `multi_score` int(0) NULL DEFAULT NULL,
  `judge_count` int(0) NULL DEFAULT NULL,
  `judge_score` int(0) NULL DEFAULT NULL,
  `saq_count` int(0) NULL DEFAULT NULL,
  `saq_score` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `repo_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  INDEX `repo_id`(`repo_id`) USING BTREE,
  CONSTRAINT `el_exam_repo_ibfk_17` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_exam_repo_ibfk_18` FOREIGN KEY (`repo_id`) REFERENCES `el_repo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_exam_repo
-- ----------------------------

-- ----------------------------
-- Table structure for el_paper
-- ----------------------------
DROP TABLE IF EXISTS `el_paper`;
CREATE TABLE `el_paper`  (
  `id` int(0) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `total_time` int(0) NULL DEFAULT NULL,
  `user_time` int(0) NULL DEFAULT NULL,
  `total_score` int(0) NULL DEFAULT NULL,
  `qualify_score` int(0) NULL DEFAULT NULL,
  `obj_score` int(0) NULL DEFAULT NULL,
  `subj_score` int(0) NULL DEFAULT NULL,
  `has_saq` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `limit_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `depart_id` int(0) NULL DEFAULT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `depart_id`(`depart_id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  CONSTRAINT `el_paper_ibfk_14` FOREIGN KEY (`depart_id`) REFERENCES `sys_depart` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_ibfk_16` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_ibfk_17` FOREIGN KEY (`depart_id`) REFERENCES `sys_depart1` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_ibfk_18` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper
-- ----------------------------

-- ----------------------------
-- Table structure for el_paper_qu
-- ----------------------------
DROP TABLE IF EXISTS `el_paper_qu`;
CREATE TABLE `el_paper_qu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL,
  `answered` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `score` int(0) NULL DEFAULT NULL,
  `actual_sorce` int(0) NULL DEFAULT NULL,
  `is_right` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `paper_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `paper_id`(`paper_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_paper_qu_ibfk_11` FOREIGN KEY (`paper_id`) REFERENCES `el_paper` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_ibfk_12` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper_qu
-- ----------------------------

-- ----------------------------
-- Table structure for el_paper_qu_answer
-- ----------------------------
DROP TABLE IF EXISTS `el_paper_qu_answer`;
CREATE TABLE `el_paper_qu_answer`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `is_right` int(0) NULL DEFAULT NULL,
  `checked` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `abc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `paper_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  `answer_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `paper_id`(`paper_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  INDEX `answer_id`(`answer_id`) USING BTREE,
  CONSTRAINT `el_paper_qu_answer_ibfk_16` FOREIGN KEY (`paper_id`) REFERENCES `el_paper` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_answer_ibfk_17` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_paper_qu_answer_ibfk_18` FOREIGN KEY (`answer_id`) REFERENCES `el_qu_answer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_paper_qu_answer
-- ----------------------------

-- ----------------------------
-- Table structure for el_qu
-- ----------------------------
DROP TABLE IF EXISTS `el_qu`;
CREATE TABLE `el_qu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL,
  `level` int(0) NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `analysis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu
-- ----------------------------

-- ----------------------------
-- Table structure for el_qu_answer
-- ----------------------------
DROP TABLE IF EXISTS `el_qu_answer`;
CREATE TABLE `el_qu_answer`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `is_right` int(0) NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `analysis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_qu_answer_ibfk_1` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu_answer
-- ----------------------------

-- ----------------------------
-- Table structure for el_qu_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_qu_repo`;
CREATE TABLE `el_qu_repo`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `qu_type` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  `repo_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  INDEX `repo_id`(`repo_id`) USING BTREE,
  CONSTRAINT `el_qu_repo_ibfk_11` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_qu_repo_ibfk_12` FOREIGN KEY (`repo_id`) REFERENCES `el_repo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_qu_repo
-- ----------------------------

-- ----------------------------
-- Table structure for el_repo
-- ----------------------------
DROP TABLE IF EXISTS `el_repo`;
CREATE TABLE `el_repo`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `multi_count` int(0) NULL DEFAULT NULL,
  `radio_count` int(0) NULL DEFAULT NULL,
  `judge_count` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_repo
-- ----------------------------

-- ----------------------------
-- Table structure for el_user_book
-- ----------------------------
DROP TABLE IF EXISTS `el_user_book`;
CREATE TABLE `el_user_book`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `wrong_count` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `qu_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `qu_id`(`qu_id`) USING BTREE,
  CONSTRAINT `el_user_book_ibfk_13` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_book_ibfk_14` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_book_ibfk_15` FOREIGN KEY (`qu_id`) REFERENCES `el_qu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_user_book
-- ----------------------------

-- ----------------------------
-- Table structure for el_user_exam
-- ----------------------------
DROP TABLE IF EXISTS `el_user_exam`;
CREATE TABLE `el_user_exam`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `try_count` int(0) NULL DEFAULT NULL,
  `max_score` int(0) NULL DEFAULT NULL,
  `passed` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `exam_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `exam_id`(`exam_id`) USING BTREE,
  CONSTRAINT `el_user_exam_ibfk_10` FOREIGN KEY (`exam_id`) REFERENCES `el_exam` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `el_user_exam_ibfk_9` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of el_user_exam
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `front_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `back_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `data_flag` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'ZBD考试系统', NULL, NULL, '1', '1', NULL, '2022-01-13 13:04:00', '2022-01-13 20:24:16');

-- ----------------------------
-- Table structure for sys_depart
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `depart_type` int(0) NULL DEFAULT NULL,
  `parent_id` int(0) NULL DEFAULT NULL,
  `depart_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `depart_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_depart
-- ----------------------------
INSERT INTO `sys_depart` VALUES (7, NULL, 0, '盘古研发中心', 'A01', 1, '2022-01-13 19:41:45', '2022-01-13 20:20:30');
INSERT INTO `sys_depart` VALUES (20, NULL, 7, '研发部', NULL, NULL, '2022-01-13 22:15:04', '2022-01-13 23:07:38');
INSERT INTO `sys_depart` VALUES (21, NULL, 20, '研发1部', NULL, NULL, '2022-01-13 22:19:49', '2022-01-13 22:19:49');
INSERT INTO `sys_depart` VALUES (22, NULL, 20, '研发2部', NULL, NULL, '2022-01-13 22:19:57', '2022-01-13 23:05:59');
INSERT INTO `sys_depart` VALUES (23, NULL, 7, '销售部', NULL, NULL, '2022-01-13 22:32:49', '2022-01-13 22:32:49');
INSERT INTO `sys_depart` VALUES (24, NULL, 23, '北方销售部', NULL, NULL, '2022-01-13 22:33:08', '2022-01-13 22:33:08');
INSERT INTO `sys_depart` VALUES (25, NULL, 23, '南方销售部', NULL, NULL, '2022-01-13 22:33:21', '2022-01-13 22:33:21');
INSERT INTO `sys_depart` VALUES (26, NULL, 23, '中部销售部', NULL, NULL, '2022-01-13 22:33:30', '2022-01-13 22:33:30');
INSERT INTO `sys_depart` VALUES (27, NULL, 0, '朱雀集中营', NULL, NULL, '2022-01-13 23:09:32', '2022-01-13 23:09:32');
INSERT INTO `sys_depart` VALUES (28, NULL, 27, '青羽营', NULL, NULL, '2022-01-13 23:09:58', '2022-01-13 23:09:58');
INSERT INTO `sys_depart` VALUES (29, NULL, 27, '黑翼营', NULL, NULL, '2022-01-13 23:10:32', '2022-01-13 23:10:32');
INSERT INTO `sys_depart` VALUES (30, NULL, 27, '饕餮营', NULL, NULL, '2022-01-13 23:15:50', '2022-01-13 23:31:31');
INSERT INTO `sys_depart` VALUES (31, NULL, 0, '测试', NULL, NULL, '2022-01-13 23:16:04', '2022-01-13 23:31:47');
INSERT INTO `sys_depart` VALUES (32, NULL, 31, '哈哈', NULL, NULL, '2022-01-13 23:20:49', '2022-01-13 23:32:19');
INSERT INTO `sys_depart` VALUES (33, NULL, 31, '呵呵', NULL, NULL, '2022-01-13 23:20:56', '2022-01-13 23:32:24');
INSERT INTO `sys_depart` VALUES (34, NULL, 31, '哎呀', NULL, NULL, '2022-01-13 23:21:02', '2022-01-13 23:32:30');
INSERT INTO `sys_depart` VALUES (35, NULL, 31, '好的', NULL, NULL, '2022-01-13 23:21:08', '2022-01-13 23:32:34');
INSERT INTO `sys_depart` VALUES (36, NULL, 31, '不行', NULL, NULL, '2022-01-13 23:21:14', '2022-01-13 23:32:41');
INSERT INTO `sys_depart` VALUES (37, NULL, 0, '在建', NULL, NULL, '2022-01-13 23:21:20', '2022-01-13 23:21:20');
INSERT INTO `sys_depart` VALUES (38, NULL, 0, '啥？', NULL, NULL, '2022-01-13 23:21:32', '2022-01-13 23:21:32');

-- ----------------------------
-- Table structure for sys_depart1
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart1`;
CREATE TABLE `sys_depart1`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `depart_type` int(0) NULL DEFAULT NULL,
  `depart_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `depart_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `parent_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `sys_depart1_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `sys_depart1` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_depart1
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'sa', '2022-01-13 12:32:57', '2022-01-14 12:33:04', NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `role_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `depart_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `data_flag` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '张太溱', '123', 'sldkf', 'sa', '7', '/public/images/user_img/default.jpg', 1, 1, '2022-01-13 13:04:00', '2022-01-15 09:56:29');
INSERT INTO `sys_user` VALUES (2, 'admin10', '哈哈乐', NULL, NULL, 'sa', '23', '/public/images/user_img/default.jpg', 0, 1, '2022-01-14 23:09:27', '2022-01-15 11:19:36');
INSERT INTO `sys_user` VALUES (3, 'admin20', '哈哈乐1', NULL, NULL, 'sa', '20', '/public/images/user_img/default.jpg', 1, 1, '2022-01-15 09:24:58', '2022-01-15 09:55:24');
INSERT INTO `sys_user` VALUES (4, 'user', '张三丰', NULL, NULL, 'sa', '22', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:58:33', '2022-01-15 10:43:16');
INSERT INTO `sys_user` VALUES (5, 'student', '张无忌', NULL, NULL, 'sa', '24', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:59:05', '2022-01-15 11:19:41');
INSERT INTO `sys_user` VALUES (6, 'teacher', '马伟明', NULL, NULL, 'sa', '22', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:59:20', '2022-01-15 10:44:27');
INSERT INTO `sys_user` VALUES (7, 'team', '提马拉丁', NULL, NULL, 'sa', '29', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:59:30', '2022-01-15 10:44:37');
INSERT INTO `sys_user` VALUES (8, 'coach', '李国强', NULL, NULL, 'sa', '22', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:59:41', '2022-01-15 10:45:05');
INSERT INTO `sys_user` VALUES (9, 'user1', '李平安', NULL, NULL, 'sa', '28', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 09:59:59', '2022-01-15 10:45:25');
INSERT INTO `sys_user` VALUES (10, 'win', '丰田年', NULL, NULL, 'sa', '25', '/public/images/user_img/default.jpg', 0, 1, '2022-01-15 10:00:12', '2022-01-15 11:19:46');
INSERT INTO `sys_user` VALUES (11, 'student11', '施图森', NULL, NULL, 'sa', '26', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 10:00:22', '2022-01-15 11:20:20');
INSERT INTO `sys_user` VALUES (12, 'ada', '安德海', NULL, NULL, 'sa', '22', '/public/images/user_img/default.jpg', 0, NULL, '2022-01-15 10:00:38', '2022-01-15 11:20:05');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `role_id` int(0) NOT NULL,
  `user_id` int(0) NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
