const jwt = require('jsonwebtoken');
const returnDataFormat = require('../common/returnDataFormat');
const jwtSecret = 'dll_jwt_Secret';

// 设置token
let setToken = function (user_name, real_name, id,avatar, role_ids, state ) {
  // console.log(role_ids, '---set token')
  return new Promise((resolve, reject) => {
    const token = jwt.sign({ user_name, real_name, id, role_ids, state, avatar }, jwtSecret, {
      expiresIn: '24h',
    });
    resolve(token);
  });
};

// token验证
let verifyToken = function (token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject(returnDataFormat.SimpleData(500, 'token为空'));
    } else {
      jwt.verify(token, jwtSecret, function (err, data) {
        if (err) {
          reject(returnDataFormat.SimpleData(500, 'token错误或已失效'));
        } else {
          resolve(returnDataFormat.SQLData(null, data, 'token验证通过'));
        }
      });
    }
  });
};

module.exports = {
  setToken,
  verifyToken,
};
