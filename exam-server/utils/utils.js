

/**
 * 随机抽取数组中指定长度元素
 * 先进行随机交换，然后截取
 * **/
module.exports.getRandomArrayElements = (arr, count) => {
    var shuffled = arr.slice(0),
      i = arr.length,
      min = i - count,
      temp,
      index;  //只是声明变量的方式, 也可以分开写
    while (i-- > min) {

        //console.log(i);
        index = Math.floor((i + 1) * Math.random()); //这里的+1 是因为上面i--的操作  所以要加回来
        temp = shuffled[index];  //即值交换
        shuffled[index] = shuffled[i];
        shuffled[i] = temp;
        //console.log(shuffled);
    }
    return shuffled.slice(min);
}

