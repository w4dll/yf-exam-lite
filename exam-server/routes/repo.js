var express = require('express');
var router = express.Router();

const repoController = require('../controller/repoController');


// 分页查询
router.post('/list', async function (req, res, next) {
  let result = await repoController.list(req.body);
  res.send(result);
});

// 删除选中repo
router.post('/delete', async function (req, res, next) {
  let result = await repoController.bulkDelete(req.body);
  res.send(result);
});

// 题库详情
router.post('/detail', async function (req, res, next) {
  let result = await repoController.repo_detail(req.body.id);
  res.send(result);
});

// 保存题库信息
router.post('/save', async function (req, res, next) {
  let result = await repoController.save(req.body);
  res.send(result);
});

// 查询全部题库信息
router.post('/list_all', async function (req, res, next) {
  let result = await repoController.list_all()
  res.send(result);
});





router.post('/batch-action', async function (req, res, next) {
  console.log('/repo/batch-action')
  let result = await repoController.batchAction(req.body);
  res.send(result);
});

module.exports = router
