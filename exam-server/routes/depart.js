var express = require('express');
var router = express.Router();

const departController = require('../controller/departController')


// 部门信息
router.post('/depart/paging', async function (req, res) {
  console.log('--------/sys/depart/paging')
  const result = await departController.pagingTree(req.body)

  res.send(result);
});

// 所有部门
router.post('/depart/all', async function (req, res) {
  console.log('/sys/depart/all')
  const result = await departController.getAll()

  res.send(result);
});

// 添加后保存
router.post('/depart/save', async function (req, res) {
  console.log('/sys/depart/save')
  const result = await departController.save_depart(req.body)

  res.send(result);
});

// 查找详细
router.post('/depart/detail', async function (req, res) {
  console.log('/sys/depart/detail')
  const result = await departController.fetch_detail(req.body)
  res.send(result);
});

// 部门树形结构
router.post('/depart/tree', async function (req, res) {
  console.log('/sys/depart/tree')
  const result = await departController.fetch_tree()
  res.send(result);
});




module.exports = router
