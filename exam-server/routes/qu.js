var express = require('express');
var router = express.Router();

const fs = require('fs')
const path = require('path')
const {Op} = require('sequelize')
const multiparty = require('multiparty');
const XLSX = require('xlsx')

const {SimpleData, SuccessData, returnValueFromToken} = require('../common/returnDataFormat');
const {sheet2blob} = require('./utils')

const Qu = require('../model/quModel')
const Repo = require('../model/repoModel')
const quController = require('../controller/quController');



// 根据条件查询所有符合条件的题目qu
router.post('/list', async function (req, res, next) {
  let result = await quController.list(req.body);
  res.send(result);
});

// 根据id查询qu的详情
router.post('/detail', async function (req, res, next) {
  let result = await quController.detail(req.body.id);
  res.send(result);
});

// 保存qu内容
router.post('/save', async function (req, res, next) {
  console.log('/qu/save')
  let result = await quController.save(req.body);
  res.send(result);
});

// 批量删除
router.post('/delete', async function (req, res, next) {
  let result = await quController.bulkDelete(req.body);
  res.send(result);
});

// 批量修改状态
router.post('/state', async function (req, res, next) {
  let result = await quController.changeState(req.body);
  res.send(result);
});

// 下载模板
router.post('/import/template', async function (req, res, next) {

  let p = path.join(process.cwd(), 'data/template.xlsx')

  fs.readFile(p, (err, data) => {
    if (err) {
      console.log(err)
      return SimpleData(400, '读取失败')
    }
    res.send(data)
  })
});

// 导入excel题库
router.post('/import/excel', async function (req, res, next) {

  let form = new multiparty.Form()
  form.parse(req, (err, fields, files) => {

    fs.readFile(files.file[0].path, (err, data) => {
      if (err) {
        console.log(err)
        return SimpleData(400, '读取失败')
      }
      let workbook = XLSX.read(data);
      let sheetNames = workbook.SheetNames
      let sheet = workbook.Sheets[sheetNames[0]]

      let jsonData = XLSX.utils.sheet_to_json(sheet)

      // todo 批量导入功能待完善

      return res.send(SuccessData(jsonData))


    })
  })
});


async function importToDB(jsonData) {

  for (let i = 0; i < jsonData.length; i++) {
    let item = jsonData[i]
    if (item['题目内容'].length > 0) {
      // 插入题目
      await Qu.create({
        qu_type: item['题目类型'],
        content: item['题目内容'],
        remark: item['选项解析'],
        analysis: item['整体解析']
      })
    } else {
      // 插入选项
    }
  }
}

// 导出数据
router.post('/export', async function (req, res, next) {

  const {content, qu_type, repoIds} = req.body

  let query = {}
  if (content) query.content = {[Op.substring]: content}
  if (qu_type) query.qu_type = qu_type

  let qus = []
  if (repoIds.length > 0) {
      qus = await Qu.findAll({
        where: query,
        attributes: {
          exclude: ['create_time', 'update_time', 'state', 'image']
        },
        include: [{
          model: Repo,
          as: 'Repo',
          attributes: [],
          required: true,
          where: {
            id: repoIds
          }
        }],
      })
    }else{
      qus = await Qu.findAll({
        where: query,
        attributes: {
          exclude: ['create_time', 'update_time', 'state', 'image']
        }
      })
    }

  let jsonData = []
  for (let i = 0; i < qus.length; i++) {
    let qu = qus[i]
    let ans = await qu.getQuAns({raw: true})

    let anList = {}
    let rightList = ''
    for (let j = 0; j < ans.length; j++) {
      let char = String.fromCharCode(65 + j)
      let an = ans[j]
      anList[char] = an.content
      if (an.is_right) rightList += char
    }
    let tmp = {...qu.dataValues, ans: rightList, ...anList}

    jsonData.push(tmp)
  }

  let sheet = XLSX.utils.json_to_sheet(jsonData)

  let d = sheet2blob(sheet)

  res.send(d)

});

// 根据examId查找用户所有错题
router.post('/wrong-qus', async function (req, res, next) {
  let user = await returnValueFromToken(req.headers.token)
  let result = await quController.getWrongs(req.body.id, user);
  res.send(result);
});





module.exports = router
