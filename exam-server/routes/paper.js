var express = require('express');
var router = express.Router();

const paperController = require('../controller/paperController');


// const path = require('path');
const {
  returnValueFromToken,
  SuccessData,
  SimpleData,
} = require('../common/returnDataFormat');


// 分页查询
router.post('/listUserExam', async function (req, res, next) {
  let result = await paperController.listUserExam(req.body);
  res.send(result);
});

// 分页查询
router.post('/list', async function (req, res, next) {
  let result = await paperController.list(req.body);
  res.send(result);
});

// 创建用户试卷
router.post('/create-paper', async function (req, res, next) {
  let exam_id = req.body.exam_id
  let user = await returnValueFromToken(req.headers.token)

  let result = await paperController.createPaper(exam_id, user);
  res.send(result);
});

// 查询试卷详情
router.post('/paper-detail', async function (req, res, next) {
  let result = await paperController.getPaperDetail(req.body.id);
  res.send(result);
});

router.post('/qu-detail', async function (req, res, next) {
  let result = await paperController.getQuDetail(req.body);
  res.send(result);
});

router.post('/fill-answer', async function (req, res, next) {
  let result = await paperController.fillAns(req.body);
  res.send(result);
});

router.post('/hand-exam', async function (req, res, next) {
  let time = parseInt(req.body.leftSeconds / 60)
  let result = await paperController.handExam(req.body.id, time);
  res.send(result);
});

router.post('/paper-result', async function (req, res, next) {
  let result = await paperController.paperResult(req.body.id);
  res.send(result);
});

router.post('/listUserPaper', async function (req, res, next) {
  let result = await paperController.listUserPaper(req.body);
  res.send(result);
});

router.post('/wrong-exams', async function (req, res, next) {
  let user = await returnValueFromToken(req.headers.token)
  let result = await paperController.getWrongExams(req.body, user);
  res.send(result);
});

// //用户注销路由
// router.get('/delete', async function (req, res) {
//   const userId = await returnValueFromToken(
//     req.headers.authorization,
//     'userId'
//   );
//   const result = await userController.deleteUser(userId);
//   res.send(result);
// });
//

//
// router.get('/findOneUser', async (req, res) => {
//   const result = await userController.findOneUser(req.query.user_id);
//   res.send(result);
// });
//
// router.post('/upload', upload.single('file'), async function (req, res, next) {
//   let userId = await returnValueFromToken(req.headers.authorization, 'userId');
//   const url = '/' + req.file.path.replace(/\\/g, '/');
//   const result = await modifyUser([userId, { avatar_url: url }]);
//   if (result.code == 200) {
//     res.send(SuccessData({ path: url }));
//   } else {
//     res.send(SimpleData(500, '图片上传失败'));
//   }
// });
//
// router.post('/pagingUsers', async (req, res) => {
//   const { offset = 1, limit, username } = req.body;
//   const result = await userController.pagingUsers({ offset, limit, username });
//   if (result) {
//     res.send(SuccessData(result));
//   } else {
//     res.send(result);
//   }
// });
module.exports = router;
