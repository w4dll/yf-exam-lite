var express = require('express');
var router = express.Router();

const { SuccessData } = require('../common/returnDataFormat');
const upload = require('../common/upload')



/* GET home page. */
// router.get('/', function (req, res, next) {
//   res.send('404');
//   next()
// });

router.post('/upload', upload.single('file'), async function (req, res, next) {
  console.log('/upload')
  const url = '/' + req.file.path.replace(/\\/g, '/');
  res.send(SuccessData({ path: url }));
});

// router.post('/upload/qu', upload.multi('file'), async function (req, res, next) {
//   console.log('/upload')
//   const url = '/' + req.file.path.replace(/\\/g, '/');
//   res.send(SuccessData({ path: url }));
// });
module.exports = router;
