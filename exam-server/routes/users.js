var express = require('express');
var router = express.Router();

const userController = require('../controller/userController');


// const path = require('path');
const {
  returnValueFromToken,
  SuccessData,
  SimpleData,
} = require('../common/returnDataFormat');

router.post('/exam/paging', async function (req, res, next) {
  let result = await userController.getExamPaging(req.body);
  res.send(result);
});


// 用户登录路由
router.post('/login', async function (req, res, next) {
  let result = await userController.userLogin(req.body);
  res.send(result);
});

// 查询用户信息路由
router.post('/info', async function (req, res) {
  let result = await returnValueFromToken(
    req.headers.token)
  res.send(SuccessData(result));
});

// 用户退出登录
router.post('/logout', (req, res, next)=>{
  res.send(SimpleData(200, 'logout success'))
})

// 用户注册路由
router.post('/reg', async function (req, res) {
  console.log('/user/reg')
  const result = await userController.addUser(req.body);
  res.send(result);
});

// 用户修改信息路由
router.post('/update', async function (req, res) {
  console.log('user/update')
  let user = null;
  if (!req.body.id){
    user = await returnValueFromToken(req.headers.token);
  }
  let id = user ? user.id : req.body.id

  const result = await userController.modifyUser(req.body, id);
  res.send(result);
});

// 查询说有用户
router.post('/paging', async function (req, res) {
  console.log('/user/paging')
  const result = await userController.getAllUsers(req.body);
  res.send(result);
});

router.post('/list', async function (req, res) {
  console.log('/user/list')
  const result = await userController.getAllUsers(req.body);
  res.send(result);
});

// 查询说有用户
router.post('/state', async function (req, res) {
  console.log('/user/state')
  const result = await userController.toggleStats(req.body.ids, req.body.state);
  res.send(result);
});

router.post('/save', async function (req, res) {
  console.log('/user/save')
  const result = await userController.addUser(req.body);
  res.send(result);
});

// //用户注销路由
// router.get('/delete', async function (req, res) {
//   const userId = await returnValueFromToken(
//     req.headers.authorization,
//     'userId'
//   );
//   const result = await userController.deleteUser(userId);
//   res.send(result);
// });
//

//
// router.get('/findOneUser', async (req, res) => {
//   const result = await userController.findOneUser(req.query.user_id);
//   res.send(result);
// });
//
// router.post('/upload', upload.single('file'), async function (req, res, next) {
//   let userId = await returnValueFromToken(req.headers.authorization, 'userId');
//   const url = '/' + req.file.path.replace(/\\/g, '/');
//   const result = await modifyUser([userId, { avatar_url: url }]);
//   if (result.code == 200) {
//     res.send(SuccessData({ path: url }));
//   } else {
//     res.send(SimpleData(500, '图片上传失败'));
//   }
// });
//
// router.post('/pagingUsers', async (req, res) => {
//   const { offset = 1, limit, username } = req.body;
//   const result = await userController.pagingUsers({ offset, limit, username });
//   if (result) {
//     res.send(SuccessData(result));
//   } else {
//     res.send(result);
//   }
// });
module.exports = router;
