const express = require('express');
const router = express.Router();

const examController = require('../controller/examController');


// 分页查询
router.post('/list', async function (req, res, next) {
  let result = await examController.listExam(req.body);
  res.send(result);
});

// 批量删除考试
router.post('/delete', async function (req, res, next) {
  let result = await examController.deleteExams(req.body.ids);
  res.send(result);
});

// 修改考试状态
router.post('/state', async function (req, res, next) {
  let result = await examController.toggleState(req.body.ids, req.body.state);
  res.send(result);
});

// 考试信息
router.post('/detail', async function (req, res, next) {
  let id = req.body.id
  let result = await examController.getDetail(id);
  res.send(result);
});

// 保存试卷设置
router.post('/save', async function (req, res, next) {
  let result = await examController.save(req.body);
  res.send(result);
});

router.post('/my-paging', async function (req, res, next) {
  let result = await examController.myPaging(req.body);
  res.send(result);
});






// router.post('/detail', async function (req, res, next) {
//   let id = req.body.id
//   let result = await examController.getDetail(id);
//   res.send(result);
// });





router.post('/repo/save', async function (req, res, next) {
  let result = await examController.repo_save(req.body);
  res.send(result);
  // res.send(SimpleData(200, 'success'))
});

// 题库查询
// router.post('/repo', async function (req, res, next) {
//   console.log('/exam/repo')
//   let result = await examController.list(req.body);
//   res.send(result);
  // res.send(SimpleData(200, 'success'))
// });


// //用户注销路由
// router.get('/delete', async function (req, res) {
//   const userId = await returnValueFromToken(
//     req.headers.authorization,
//     'userId'
//   );
//   const result = await userController.deleteUser(userId);
//   res.send(result);
// });
//

//
// router.get('/findOneUser', async (req, res) => {
//   const result = await userController.findOneUser(req.query.user_id);
//   res.send(result);
// });
//
// router.post('/upload', upload.single('file'), async function (req, res, next) {
//   let userId = await returnValueFromToken(req.headers.authorization, 'userId');
//   const url = '/' + req.file.path.replace(/\\/g, '/');
//   const result = await modifyUser([userId, { avatar_url: url }]);
//   if (result.code == 200) {
//     res.send(SuccessData({ path: url }));
//   } else {
//     res.send(SimpleData(500, '图片上传失败'));
//   }
// });
//
// router.post('/pagingUsers', async (req, res) => {
//   const { offset = 1, limit, username } = req.body;
//   const result = await userController.pagingUsers({ offset, limit, username });
//   if (result) {
//     res.send(SuccessData(result));
//   } else {
//     res.send(result);
//   }
// });
module.exports = router;
