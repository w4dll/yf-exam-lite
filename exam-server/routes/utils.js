const XLSX = require('xlsx')
const {Buffer} = require('buffer')



// sheet表单转blob数据
module.exports.sheet2blob = (sheet, sheetName) => {

  sheetName = sheetName || 'sheet1';
  let workbook = {
    SheetNames: [sheetName],
    Sheets: {}
  };

  workbook.Sheets[sheetName] = sheet;

  // 生成excel的配置项
  let wopts = {
    bookType: 'xlsx',  // 要生成的文件类型
    bookSST: false,    // 是否生成Shared String Table，官方解释是，如果开启生成速度会下降，但在低版本IOS设备上有更好的兼容性
    type: 'binary'
  };

  let wbout = XLSX.write(workbook, wopts);

  return  new Buffer.from(wbout, 'binary')
}
