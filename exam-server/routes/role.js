var express = require('express');
var router = express.Router();

const roleController = require('../controller/roleController');


// 获取所有角色
router.post('/role/paging', async function (req, res, next) {
  console.log('/sys/role/paging')
  let result = await roleController.getRolePaging(req.body);
  res.send(result);
});

// 获取所有角色
router.post('/role/list', async function (req, res, next) {
  console.log('/sys/role/list')
  let result = await roleController.listAllRole();
  res.send(result);
});

router.post('/role/add', async function (req, res, next) {
  console.log('/sys/role/add')
  let result = await roleController.addRole(req.body);
  res.send(result);
});

module.exports = router
