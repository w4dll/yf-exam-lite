const {SimpleData, SuccessData, returnValueFromToken} = require('../common/returnDataFormat');
const {setToken} = require('../utils/token');
const Exam = require('../model/examModel');
const ExamRepo = require('../model/examRepoModel');
const Paper = require('../model/paperModel')
const PaperQu = require('../model/paperQuModel')
const PaperQuAns = require('../model/paperQuAnsModel')
const Qu = require('../model/quModel')
const QuAns = require('../model/quAnsModel')
const Repo = require('../model/repoModel')
const User = require('../model/userModel')
const UserExam = require('../model/userExamModel')

const {getRandomArrayElements} = require('../utils/utils')


const {Op} = require('sequelize');
const sequelize = require('../config/sequelize')


// 查询考生的考试信息
module.exports.listUserExam = async (listQuery) => {

  const {user_id, exam_id} = listQuery.params

  let userExams = await UserExam.findAll({
    where: {
      user_id: user_id,
      exam_id: exam_id
    },
    raw: true
  })

  if (userExams) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = listQuery.size * listQuery.current
    let dd = userExams.slice(start, end)

    return SuccessData({records: dd, total: userExams.length})
  } else {
    return SimpleData(400, '获取失败')
  }
}

// 查询考试信息
module.exports.list = async (listQuery) => {

  let query = {}
  const {open_type, title} = listQuery.params
  if (open_type) query.open_type = open_type
  if (title) query.title = {[Op.substring]: title}

  let result = await Exam.findAll({
    where: query,
    raw: true,
  });

  if (result) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = listQuery.size * listQuery.current
    let dd = result.slice(start, end)

    return SuccessData({records: dd, total: result.length});
  } else {
    return SimpleData(500, '试卷信息不存在');
  }
};

function getRandomData(arr, paperId, type, count, start = 0) {
  let radioList = arr.filter(item => item['repo.qus.qu_type'] === type)
    .map(item => {
      return item['repo.qus.id']
    })
  const {radio_score, multi_score, judge_score} = arr[0]
  let s = [radio_score, multi_score, judge_score]

  let radioList1 = getRandomArrayElements(radioList, count)
  return radioList1.map((item, index) => {
    return {
      qu_type: type,
      paper_id: paperId,
      qu_id: item,
      sort: index + start,
      score: s[type - 1]
    }
  })
}

// 获取考试id对应的题库信息
module.exports.createPaper = async (exam_id, user) => {

  // 1. 事务，创建失败回滚
  const t = await sequelize.transaction()

  try {

    // 考试信息写入试卷信息中
    let examInfo = (await Exam.findByPk(exam_id)).get({plain: true})

    let U = (await User.findByPk(user.id)).get({plain: true})

    // 2. 创建试卷
    let paper = await Paper.create({
      exam_id: exam_id,
      user_id: user.id,
      title: examInfo.title,
      total_score: examInfo.total_score,
      total_time: examInfo.total_time,
      qualify_score: examInfo.qualify_score,
      depart_id: U.depart_id,
    }, {
      transaction: t
    })

    // 3. 获取考试与题库关联信息
    let exam = await Exam.findAll({
      where: {id: exam_id},
      attributes: {
        exclude: ['update_time', 'create_time']
      },
      include: [
        {
          model: ExamRepo,
          attributes: {
            exclude: ['update_time', 'create_time', 'exam_id', 'repo_id']
          }
        }
      ],
      raw: true
    })

    // 4. 每一个关联信息，随机抽取指定数量题目,并插入到对应试卷中
    for (let i = 0; i < exam.length; i++) {
      let examRepo = exam[i]

      // 问题列表
      let qus = await ExamRepo.findAll({
        where: {id: examRepo['exam_repos.id']},
        attributes: {
          exclude: ['update_time', 'create_time',]
        },
        raw: true,
        include: [
          {
            model: Repo,
            include: [
              {
                model: Qu,
                attributes: ['id', 'qu_type']
              }
            ]
          }
        ]
      })

      let radios = getRandomData(qus, paper.id, 1, examRepo.radio_count)
      let multis = getRandomData(qus, paper.id, 2, examRepo.multi_count, radios.length)
      let judges = getRandomData(qus, paper.id, 3, examRepo.judge_count, radios.length + multis.length)

      // 批量创建
      await PaperQu.bulkCreate(radios, {transaction: t})
      await PaperQu.bulkCreate(multis, {transaction: t})
      await PaperQu.bulkCreate(judges, {transaction: t})
    }
    await t.commit()

    return SuccessData(paper.get({plain: true}))

  } catch (err) {
    console.log(err)
    await t.rollback()
  }
}

// 获取paper对应的所有qu，放入对象中
module.exports.getPaperDetail = async (id) => {
  let res = await PaperQu.findAll({
    where: {paper_id: id},
    raw: true,
  })

  let paper = await Paper.findByPk(id)
  let leftSeconds = paper.get({plain: true}).total_time * 60

  return SuccessData({
    radioList: res.filter(item => item.qu_type === 1),
    multiList: res.filter(item => item.qu_type === 2),
    judgeList: res.filter(item => item.qu_type === 3),
    leftSeconds: leftSeconds
  })

}

// 给定quId,获取answers
module.exports.getQuDetail = async (param) => {

  // 1. 根据paperId和quId得到一条记录，记录是否答题、得分等信息
  let res = await PaperQu.findOne({
    where: param,
    include: [{
      model: Qu,
      include: {
        model: QuAns,
        as: 'QuAns'
      }
    }]
  })

  console.log(res)
  if (res) {
    let dd = res.qu.QuAns.map((item, index) => {
      return {
        ...param, answer_id: item.id, abc: String.fromCharCode(65 + index),
        is_right: item.is_right
      }
    })

    let ansList = []
    for (let i = 0; i < dd.length; i++) {
      let [model, done] = await PaperQuAns.findOrCreate({
        where: dd[i],
        defaults: dd[i],
        attributes: {exclude: ['create_time', 'update_time']},
      })
      ansList.push({abc: dd.abc, ...model.dataValues, content: res.qu.QuAns[i].content})
    }

    let data = {
      id: res.dataValues.id,
      content: res.qu.content,
      sort: res.dataValues.sort,
      qu_type: res.dataValues.qu_type,
      answerList: ansList
    }
    return SuccessData(data)
  } else {
    return SimpleData(400, '获取失败')
  }
}


// 填充答案
module.exports.fillAns = async (params) => {

  // 1. 事务回滚
  let t = await sequelize.transaction()

  // 2. 该题已作答，标记
  await PaperQu.update({answered: 1}, {
    where: {
      paper_id: params.paper_id,
      qu_id: params.qu_id,
    },
    transaction: t
  })

  // 3. 标记考生答案
  await PaperQuAns.update({checked: 0}, {
    where: {
      paper_id: params.paper_id,
      qu_id: params.qu_id,
    },
    transaction: t
  })

  // 4. 填充考生答案
  for (let i = 0; i < params.answers.length; i++) {
    await PaperQuAns.update({checked: 1}, {
      where: {
        id: params.answers[i]
      },
      transaction: t,
    })
  }

  // 5.判断该题得分
  let myAns = await PaperQuAns.findAll({
    where: {
      paper_id: params.paper_id,
      qu_id: params.qu_id,
    },
    raw: true
  })
  let tmp = myAns.map(item => item.is_right === item.checked)
  console.log(tmp)
  let flag = tmp.every(item => item)
  if (flag) {
    // 得分
    console.log('得分')
  }

  try {
    await t.commit()
    return SimpleData(200, '提交成功！')
  } catch {
    await t.rollback()
  }
}

// 提交试卷
module.exports.handExam = async (paperId, time) => {

  let totalScore = 0

  // 试卷实例
  let paper = await Paper.findByPk(paperId)

  let paperQus = await paper.getPaperQus()

  for (let i = 0; i < paperQus.length; i++) {
    let pqs = paperQus[i]

    let pqa = await PaperQuAns.findAll({
      where: {
        qu_id: pqs.qu_id,
        paper_id: pqs.paper_id
      }
    })

    let rList = pqa.map(item => {
      return item.is_right === item.checked
    })
    let r = rList.every(item => item)

    if (r) {
      pqs.actual_score = pqs.score
      totalScore += pqs.score
    }
    pqs.is_right = r
    pqs.save()
  }

  let [uExam, isCreated] = await UserExam.findOrCreate({
    where: {
      user_id: paper.user_id,
      exam_id: paper.exam_id
    },
    defaults: {
      try_count: 0,
      max_score: 0,
      passed: 0
    }
  })

  // 保存user_exam
  uExam.try_count += 1
  if (totalScore > uExam.max_score) uExam.max_score = totalScore
  if (!uExam.passed) uExam.passed = paper.qualify_score <= totalScore
  await uExam.save()

  // 更新保存试卷
  paper.user_score = totalScore
  paper.user_time = paper.total_time - time
  await paper.save()


  return SimpleData(200, 'success')
}


// 考试结果
module.exports.paperResult = async (paperId) => {

  let paper = await Paper.findByPk(paperId)
  let user = await paper.getUser()

  let paperQus = await paper.getPaperQus({
    attributes: {exclude: ['create_time', 'update_time', 'qu_id']},
    include: [{
      model: Qu,
    }]
  })

  let anList = []
  for (let i = 0; i < paperQus.length; i++) {
    let qu = paperQus[i].qu
    let ans = await qu.getPaperQuAns({
      where: {paper_id: paperId},
      include: [{
        model: QuAns
      }],
    })

    let ans1 = ans.map(item => {
      return {
        abc: item.abc,
        checked: item.checked,
        is_right: item.is_right,
        id: item.id,
        content: item.qu_an.dataValues.content,
        image: item.qu_an.dataValues.image,
      }
    })

    anList.push({
      answerList: ans1,
      id: qu.dataValues.id,
      sort: paperQus[i].dataValues.sort,
      actual_score: paperQus[i].actual_score,
      content: qu.dataValues.content,
      image: qu.dataValues.image,
      qu_type: qu.dataValues.qu_type,
      answered: paperQus[i].dataValues.answered,
    })
  }

  if (paper) {
    let resData = {
      userRealName: user.dataValues.real_name,

      user_time: paper.dataValues.user_time,
      user_score: paper.dataValues.user_score,
      create_time: paper.dataValues.create_time,
      title: paper.dataValues.title,


      quList: anList,
    }
    return SuccessData(resData)
  } else {
    return SimpleData(200, 'success')
  }

}

module.exports.listUserPaper = async (params) => {

  let papers = await Paper.findAll({
    where: params
  })

  console.log(papers)
  if (papers) {
    return SuccessData({records: papers})
  } else {
    return SimpleData(200, 'success')
  }
}


module.exports.getWrongExams = async (data, user) => {

  const { current, size, params} = data
  console.log(data, user)

  let res = await Paper.findAll({
    where: {
      exam_id: params.exam_id,
      user_id: user.id
    },
    include:[{
      model:PaperQu,
      as:'PaperQus',
      where:{
        is_right:0
      }
    }]
  })

  var data = {}
  for(let i=0; i<res.length; i++){
    let paper = res[i]
    let paperQus = paper.PaperQus

    for(let j=0; j<paperQus.length; j++){
      let paperQu = paperQus[j]
      let qu_id = paperQu.qu_id

      if (qu_id in data){
        data[qu_id].wrong_count += 1
      }else{
        data[qu_id] = {}
        data[qu_id].wrong_count = 1
        data[qu_id].title = (await paperQu.getQu()).content
        data[qu_id].update_time = (await paperQu.getQu()).update_time
        data[qu_id].id = (await paperQu.getQu()).id
      }
    }
  }

  let d = Object.values(data)
  console.log(d)

  if (data) {
    return SuccessData({records: d, total:d.length})
  } else {
    return SimpleData(200, 'success')
  }
}
