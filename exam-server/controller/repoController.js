const Repo = require('../model/repoModel');
const QuRepo = require('../model/quRepoModel');
const Qu = require('../model/quModel');

const {Op} = require('sequelize')
const sequelize = require('../config/sequelize')

const {SimpleData, SuccessData} = require('../common/returnDataFormat');
const {updateRepoCount} = require('./utils')



// 分页查询
module.exports.list = async (listQuery) => {

  let query = listQuery.params.title !== '' ? {title: {[Op.substring]: listQuery.params.title}} : {}

  let result = await Repo.findAll({
    where: query,
    raw: true,
  });

  if (result) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = listQuery.size * listQuery.current

    let dd = result.slice(start, end)
    return SuccessData({records: dd, total: result.length});
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

// 根据repoId，批量删除题库
module.exports.bulkDelete = async (data) => {

  let t = await sequelize.transaction()

  try {
    await Repo.destroy({
      where: {id: data.ids},
      transaction:t
    })
    await t.commit()

    return SimpleData(200, '删除失败')
  }catch (err){

    await t.rollback()

    return SimpleData(400, '删除失败')
  }
}

// 根据id获得题库详情
module.exports.repo_detail = async (id) => {

  let repo = await Repo.findByPk(id)

  if (repo) {
    return SuccessData(repo.get({plain:true}));
  } else {
    return SimpleData(500, '题库信息不存在');
  }
}

// 保存
module.exports.save = async (data) => {
  let res;
  // 如果id存在为更新，否则为创建
  if (data.id) {
    res = await Repo.update(data, {
      where: {id: data.id}
    })
  } else {
    res = await Repo.create(data)
  }
  if (res) {
    return SuccessData(res)
  } else {
    return SimpleData(200, '保存失败')
  }
}

// 查询全部
module.exports.list_all = async () => {

  let result = await Repo.findAll({
    raw: true,
  });

  if (result) {
    return SuccessData(result);
  } else {
    return SimpleData(500, '查询信息为空');
  }
};




module.exports.batchAction = async (data) => {
  let repos = data.repoIds
  let quIds = data.quIds

  console.log(quIds, repos)
  try {
    if (data.remove) {

      // 删除
      for (let i = 0; i < repos.length; i++) {
        await QuRepo.destroy({
          where: {
            qu_id: quIds,
            repo_id: repos[i],
          }
        })

        await updateRepoCount(repos[i])
      }
    } else {

      // 加入
      for (let m = 0; m < repos.length; m++) {
        for (let n = 0; n < quIds.length; n++) {

          let qu = await Qu.findByPk(quIds[n])

          await QuRepo.findOrCreate({
            where: {
              qu_id: quIds[n],
              repo_id: repos[m]
            },
            defaults: {
              qu_id: quIds[n],
              repo_id: repos[m],
              qu_type: qu.qu_type
            },
          })
        }

        // 将所有qu添加到repo中，更新repo中各类型题目数量
        await updateRepoCount(repos[m])
      }

    }
    return SimpleData(200, '操作成功！')
  } catch {
    return SimpleData(400, '操作失败！')
  }

}

