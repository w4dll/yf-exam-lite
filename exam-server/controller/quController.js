const {SimpleData, SuccessData} = require('../common/returnDataFormat');
const {updateRepoCount} = require('./utils')

const Qu = require('../model/quModel');
const PaperQu = require('../model/paperQuModel');
const Paper = require('../model/paperModel');
const Repo = require('../model/repoModel');
const QuRepo = require('../model/quRepoModel');
const QuAns = require('../model/quAnsModel');

const {Op} = require('sequelize')


// 根据条件查询题目 qu
module.exports.list = async (listQuery) => {
  const {content, qu_type, repoIds} = listQuery.params

  let query = {}
  if (content) query['content'] = {[Op.substring]: content}
  if (qu_type) query['qu_type'] = qu_type

  let result = null;
  let query1 = {}
  if (repoIds.length > 0) {
    query1['id'] = repoIds.length > 1 ? repoIds : repoIds[0]
    result = await Qu.findAll({
      where: query,
      include: [{
        model: Repo,
        as: 'Repo',
        where: query1,
        required: true
      }],
      raw: true,
    });
  } else {
    result = await Qu.findAll({
      where: query,
      raw: true,
    });
  }

  if (result) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = listQuery.size * listQuery.current

    let dd = result.slice(start, end)
    return SuccessData({records: dd, total: result.length});
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

// 保存 qu
module.exports.save = async (data) => {

  try {

    if (data.id) {

      // update
      let qu = await Qu.findByPk(data.id)

      // 1. 更新qu
      qu.qu_type = data.qu_type
      qu.level = data.level
      qu.content = data.content
      qu.analysis = data.analysis
      qu.save()

      // 2. 更新qu相关的选项，先删除、后添加
      await QuAns.destroy({
        where: {qu_id: data.id}
      })

      for (let i = 0; i < data.answerList.length; i++) {
        await qu.createQuAn(data.answerList[i])
      }

      // 3. 更新quRepo，修改qu与repo对应关系
      let repo = (await qu.getRepo()).map(item => item.id)

      await QuRepo.destroy({
        where: {qu_id: data.id}
      })

      // 删除后要更新题库基本信息
      for (let i = 0; i < repo.length; i++) {
        await updateRepoCount(repo[i])
      }

      // 添加后也要更新
      for (let i = 0; i < data.repoIds.length; i++) {
        let repoId = data.repoIds[i]
        await QuRepo.create({qu_id: data.id, repo_id: repoId})

        await updateRepoCount(repoId)
      }
    } else {
      let qu = await Qu.create({
        qu_type: data.qu_type,
        level: data.level,
        content: data.content,
        analysis: data.analysis,
      })
      let quId = qu.dataValues.id

      let aa = data.repoIds.map(id => {
        return {qu_id: quId, repo_id: id}
      })
      await QuRepo.bulkCreate(aa)

      let dd = data.answerList.map(item => {
        return {qu_id: quId, ...item}
      })
      await QuAns.bulkCreate(dd)
    }
    return SuccessData(200, 'success')
  } catch (err) {
    console.log(err)
    return SimpleData(400, '添加题目失败！')
  }

}

// qu 的详情
module.exports.detail = async (quId) => {

  // 1. qu实例
  let qu = await Qu.findByPk(quId)

  if (qu) {

    let ans = await qu.getQuAns({raw: true})
    let repos = await qu.getRepo({raw: true})

    let answerList = ans.map(item => {
      return {
        is_right: !!item.is_right,
        content: item.content,
        analysis: item.analysis,
        id: item.id,
      }
    })
    let repoIds = repos.map(item => item.id)
    return SuccessData({...qu.get({palin: true}), answerList, repoIds})
  } else {
    return SimpleData(200, '获取失败！')
  }
}

// 批量删除 qu
module.exports.bulkDelete = async (data) => {

  let ids = data.ids

  try {
    for (let i = 0; i < ids.length; i++) {
      await Qu.destroy({
        where: {id: ids[i]}
      })

      await QuRepo.destroy({
        where: {qu_id: ids[i]}
      })

      await QuAns.destroy({
        where: {qu_id: ids[i]}
      })
    }
    return SimpleData(200, '删除成功')
  } catch (err) {
    return SimpleData(400, '删除失败')
  }
}

// 批量修改状态
module.exports.changeState = async (data) => {

  const {ids, state} = data

  try {
    for (let i = 0; i < ids.length; i++) {
      await Qu.update({state: state}, {
        where: {id: ids[i]}
      })
    }
    return SimpleData(200, 'success')

  } catch {
    return SimpleData(400, '更改是失败！')
  }
}


module.exports.getWrongs = async (examId, user) => {

  let papers = await Paper.findAll({
    where: {
      exam_id: examId,
      user_id: user.id
    },
    include:[{
      model:PaperQu,
      as:'PaperQus',
      where:{
        is_right:0
      }
    }]
  })

  let quList = []
  for(let i=0; i<papers.length; i++){
    let paper = papers[i]
    let paperQus = paper.PaperQus
    paperQus.forEach(item=>{
      if (quList.indexOf(item.qu_id) === -1) quList.push(item.qu_id)
    })
  }
  // console.log(quList)

  return SuccessData(quList)
}
