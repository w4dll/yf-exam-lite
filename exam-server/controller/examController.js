const {SimpleData, SuccessData} = require('../common/returnDataFormat');
const ExamRepo = require('../model/examRepoModel');
const ExamDepart = require('../model/examDepartModel');
const Exam = require('../model/examModel')
const Repo = require('../model/repoModel')
const Depart = require('../model/departModel')
const UserExam = require('../model/userExamModel')

const sequelize = require('../config/sequelize')
const {Op} = require('sequelize')

// 分页查询考试信息
module.exports.listExam = async (listQuery) => {

  let {open_type, start_time, end_time, title} = listQuery.params

  let query = {}
  if (open_type) query.open_type = open_type
  if (!!title) query.title = {[Op.substring]: title}
  if (start_time) query.start_time = {[Op.gte]: start_time}
  if (end_time) query.end_time = {[Op.lte]: end_time}

  let res = await Exam.findAll({
    where: query,
    raw: true
  })

  if (res) {

    let start = (listQuery.current - 1) * listQuery.size
    let end = listQuery.current * listQuery.size
    let dd = res.slice(start, end)

    return SuccessData({records: dd, total: res.length})
  } else {
    return SimpleData(400, '获取失败')
  }
}

// 批量删除考试信息
module.exports.deleteExams = async (ids) => {

  let t = await sequelize.transaction()

  try {
    await Exam.destroy({
      where: {id: ids},
      transaction: t,
    })
    await t.commit()
    return SimpleData(200, '删除成功！')
  } catch (err) {
    await t.rollback()
    return SimpleData(400, '删除失败！')
  }

}

// 修改状态
module.exports.toggleState = async (ids, state) => {

  let t = await sequelize.transaction()

  try {
    await Exam.update({state: state}, {
      where: {id: ids},
      transaction: t,
    })

    await t.commit()
    return SimpleData(200, '修改成功！')

  } catch (err) {

    await t.rollback()
    return SimpleData(400, '修改失败！')
  }
}

// 获取考试详情
module.exports.getDetail = async (id) => {

  let res = await Exam.findByPk(id, {
    attributes: {
      exclude: ['create_time', 'update_time']
    }
  })

  let repos = await res.getExam_repos({
    raw: true,
    attributes: {
      exclude: ['create_time', 'update_time']
    }
  })
  let departs = await res.getDeparts({raw: true})


  if (res) {
    let result = {
      ...res.get({plain: true}),
      repoList: repos,
      departIds: departs.map(item => item.id)
    }
    return SuccessData(result)
  } else {
    return SimpleData(400, '获取失败')
  }
}

// 保存添加的考试信息
module.exports.save = async (data) => {
  console.log(data)
  let repoList = data.repoList
  let quList = data.quList
  let departIds = data.departIds

  delete data.repoList
  delete data.quList
  delete data.departIds

  // 新建事务
  const t = await sequelize.transaction();

  try {

    // id存在表示更新，不存在表示创建
    let id, exam;
    if (data.id) {
      id = data.id
      await Exam.update(data, {
        where: {id: id},
        transaction: t
      })

      exam = await Exam.findByPk(id, {
        include: Depart
      })

      await ExamDepart.destroy({
        where: {
          exam_id: id
        },
        transaction: t
      })

      // 删除考试、题库对应关系
      await ExamRepo.destroy({
        where: {exam_id: id},
        transaction: t
      })
    } else {
      exam = await Exam.create(data, {transaction: t})
      id = newExam.dataValues.id
    }

    // 添加考试和部门对应关系,部门开放的时候才添加
    if (data.open_type === 2) {
      for (let i = 0; i < departIds.length; i++) {
        let depart = await Depart.findByPk(departIds[i])
        await exam.addDepart(depart, {transaction: t})
      }
    }

    // 添加考试和题库对应关系
    for (let i = 0; i < repoList.length; i++) {
      await ExamRepo.create({
        ...repoList[i],
        exam_id: id
      }, {
        transaction: t
      })
    }

    // 提交事务
    await t.commit()

    return SimpleData(200, 'success')
  } catch {

    // 错误，回撤
    await t.rollback()

    return SimpleData(200, '创建失败')
  }
}


module.exports.myPaging = async (listQuery) => {

  let {title} = listQuery.params

  let result = await UserExam.findAll({
    include:[{
      model:Exam,
      where:{
        title:{[Op.substring]:title}
      }
    }]
  })
  console.log(result)

   if (result) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = start + listQuery.size
    let dd = result.slice(start, end)
    return SuccessData({records: dd, total: result.length});
  } else {
    return SimpleData(500, '题库信息不存在');
  }


}


// 获取考试详情
module.exports.getExamInfo = async (id) => {
  let res = await Exam.findByPk(id)
  if (res) {
    return SuccessData(res.get({plain: true}))
  } else {
    return SimpleData(400, '获取失败')
  }
}


// 根据条件查询试卷
module.exports.list = async (listQuery) => {
  let query = {}, query1 = {}
  Object.keys(listQuery.params).forEach(key => {
    if (listQuery.params[key]) {
      if (['title', 'content', 'create_time'].includes(key)) {
        query1[key] = listQuery.params[key]
      } else {
        query[key] = listQuery.params[key]
      }
    }
  })

  let result = await ExamRepo.findAll({
    where: query,
    attributes: {
      exclude: ['create_time', 'update_time'] // 排除
    },
    include: [{ // 关联查询
      model: Exam,
      attributes: ['title', 'content', 'create_time'],
      where: query1
    }],
    raw: true,
  });
  console.log(result, '----------------------------111')
  if (result) {
    let start = listQuery.size * (listQuery.current - 1)
    let end = start + listQuery.size
    let dd = result.slice(start, end)
    return SuccessData({records: dd, total: result.length});
  } else {
    return SimpleData(500, '题库信息不存在');
  }
};


// 保存题库
module.exports.repo_save = async (params) => {
  let result
  if (params.id) {
    result = await Repo.update(params, {
      where: {id: params.id},
    })
  } else {
    result = await Repo.create(params)
  }

  if
  (result) {
    return SuccessData(result);
  } else {
    return SimpleData(500, '题库信息不存在');
  }
}

// // 添加用户
// module.exports.addUser = async (params) => {
//   // 设置默认的头像
//   if (!params.avatar) params.avatar = '/public/images/user_img/default.jpg'
//   if (!params.role_ids) params.role_ids = 'student'
//
//   console.log(params, '-------params')
//   const user = await this.selectUserByName(params.user_name);
//   if (user.success) {
//     return {
//       msg: '用户已存在',
//     };
//   } else {
//     const result = (await User.create(params)).get({ plain: true });
//     if (result) return SimpleData(200, '用户添加成功');
//   }
// };
//
// // 通过用户名查询用户信息函数
// module.exports.selectUserByName = async (user_name) => {
//   const result = await User.findOne({
//     where: {
//       user_name,
//     },
//   });
//   if (result) {
//     return SuccessData(result.get({ plain: true }));
//   } else {
//     return SimpleData(false, '用户不存在');
//   }
// };
//
// //修改用户
// module.exports.modifyUser = async (params) => {
//   const result = await User.update(
//     { ...params[0] },
//     {
//       where: {
//         id: params[1],
//       },
//     }
//   );
//   if (result[0]) {
//     return SimpleData(200, '修改用户成功');
//   } else {
//     return SimpleData(500, '用户修改失败');
//   }
// };


// //根据id查询用户
// module.exports.findOneUser = async (user_id) => {
//   const result = (
//     await User.findOne({
//       where: {
//         user_id,
//       },
//       include: [
//         {
//           model: Topic,
//           as: 'topic',
//           include: [
//             {
//               model: Comment,
//               attributes: ['id'],
//             },
//           ],
//         },
//       ],
//       attributes: {
//         exclude: ['created_time', 'update_time'],
//       },
//     })
//   ).get({ plain: true });
//   return SuccessData(result);
// };


// //验证用户是否已经存在
// module.exports.validateUser = async (username) => {
//   const cantUpdate = await User.findOne({
//     where: {
//       username,
//     },
//     raw: true,
//   });
//   if (cantUpdate) {
//     return SimpleData(500, '该用户名已存在');
//   } else {
//     return SimpleData(200, '用户名不存在');
//   }
// };
//

// // 删除用户
// module.exports.deleteUser = async (userId) => {
//   const result = await User.destroy({
//     where: {
//       user_id: userId,
//     },
//   });
//   if (result) {
//     return SimpleData(200, '删除用户成功');
//   } else {
//     return SimpleData(500, '删除用户失败');
//   }
// };
//
// //分页查询用户
// module.exports.pagingUsers = async ({ offset, limit, username }) => {
//   if (limit) {
//     offset = (offset - 1) * limit;
//   } else {
//     offset = 0;
//   }
//   const result = await User.findAndCountAll({
//     where: {
//       username: {
//         [sequelize.Op.like]: '%' + username + '%',
//       },
//     },
//     offset,
//     limit,
//   });
//   if (result) {
//     return result;
//   } else {
//     return SimpleData(500, '获取用户列表失败');
//   }
// };
