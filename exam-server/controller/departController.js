const { SimpleData, SuccessData } = require('../common/returnDataFormat');
const { setToken } = require('../utils/token');
const Depart = require('../model/departModel');


function sortArr(arr) {
  var copy = JSON.parse(JSON.stringify(arr));
  var obj = {};
  copy.forEach((item, index) => {
    obj[item.id] = item;
  });
  var res = [];
  copy.forEach(item => {
    if (item.parent_id === 0) {
      res.push(item);
    }
    for (let key in obj) {
      if (item.id == obj[key].parent_id) {
        if (item.children) {
          item.children.push(obj[key]);
        } else {
          item.children = [obj[key]];
        }
      }
    }
  });
  return res;
}

module.exports.fetch_tree = async ()=>{
  let result = await Depart.findAll({
    raw:false,
  });
  if (result) {
    let d = sortArr(result)
    return SuccessData(d);
  } else {
    return SimpleData(500, '查询信息为空');
  }
}

// 查询所有部门信息
module.exports.pagingTree = async (listQuery) => {
  let result = await Depart.findAll({
    raw:true,
  });
  console.log(result)
  if (result) {
    let d = sortArr(result)

    let start = listQuery.current-1
    let end = listQuery.size + start
    let dd = d.slice(start,end)


    console.log(dd, d)
    return SuccessData({records:dd, total:d.length});
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

// 查询所有部门信息
module.exports.getAll = async () => {
  let result = await Depart.findAll({
    raw:false,
  });
  if (result) {
    return SuccessData(result);
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

// 保存
module.exports.save_depart = async (params) => {

  let result = null
  console.log(params)
  if (params.id) {
    result = await Depart.update(params, {
      where:{id:params.id}
    })
  }else {
    result = await Depart.create(params, {raw:true});
  }
  // console.log(result)
  if (result) {
    return SuccessData(result);
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

// 查询某一个
module.exports.fetch_detail = async (params) => {
  let result = await Depart.findOne({
    where:params
  });
  if (result) {
    return SuccessData(result.get({plain:true}));
  } else {
    return SimpleData(500, '查询信息为空');
  }
};




