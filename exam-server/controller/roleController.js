const { SimpleData, SuccessData } = require('../common/returnDataFormat');
const Role = require('../model/roleModel');

// 查询所有
module.exports.getRolePaging = async (listQuery) => {
  let limit = listQuery.size
  let offset = (listQuery.current-1)*limit

  let result = await Role.findAll({
    raw:true,
  });
  if (result) {
    let dd = result.slice(offset, offset+limit)
    return SuccessData({records:dd, total:result.length});
  } else {
    return SimpleData(500, '查询信息为空');
  }
};



module.exports.listAllRole = async () => {
  let result = await Role.findAll({
    raw:false,
  });
  if (result) {
    return SuccessData(result);
  } else {
    return SimpleData(500, '查询信息为空');
  }
};

module.exports.addRole = async (params)=>{
   const role = await this.selectRoleByName(params.role_name);
  if (role.success) {
    return {
      msg: '用户已存在',
    };
  } else {
    const result = (await Role.create(params)).get({ plain: true });
    if (result) return SimpleData(200, '角色添加成功');
  }

}

module.exports.selectRoleByName = async (role_name) => {
  const result = await Role.findOne({
    where: {
      role_name,
    },
  });
  if (result) {
    return SuccessData(result.get({ plain: true }));
  } else {
    return SimpleData(false, '用户不存在');
  }
};
