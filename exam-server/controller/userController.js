const {SimpleData, SuccessData} = require('../common/returnDataFormat');
const {setToken} = require('../utils/token');
const User = require('../model/userModel');
const UserExam = require('../model/userExamModel');
const {Op} = require("sequelize");

// const sequelize = require('sequelize');

// 详情
module.exports.getExamPaging = async (listQuery) => {

    const {exam_id, real_name} = listQuery.params
    let query1 = exam_id ? {exam_id:exam_id}: {}
    let query2 = real_name ? {real_name:{[Op.substring]:real_name}} : {}

    let start = listQuery.size*(listQuery.current-1)
    let end = listQuery.size * listQuery.current

    let result = await UserExam.findAll({
        where: query1,
        include:[{
            model:User,
            where:query2,
            attributes:['real_name']
        }],
        raw: true,
    });

    if (result) {
        let dd = result.slice(start, end)
        return SuccessData({total: result.length, records: dd});
    } else {
        return SimpleData(500, '查询信息为空');
    }

}

// 登录用户
module.exports.userLogin = async (params) => {

    // todo 密码没有进行处理查询
    let result = await User.findOne({
        where: {user_name: params.username},
    });
    if (result) {
        result = result.get({plain: true});
        const token = await setToken(
            result.user_name,
            result.real_name,
            result.id,
            result.avatar,
            result.role_ids,
            result.state
        );
        return SuccessData({
            token,
        });
    } else {
        return SimpleData(500, '用户名或密码存在错误');
    }
};

// 添加用户
module.exports.addUser = async (params) => {
    // 设置默认的头像
    if (!params.avatar) params.avatar = '/public/images/user_img/default.jpg'
    if (!params.role_ids) params.role_ids = 'student'

    console.log(params, '-------params')
    const user = await this.selectUserByName(params.user_name);
    if (user.success) {
        return {
            msg: '用户已存在',
        };
    } else {
        const result = (await User.create(params)).get({plain: true});
        if (result) return SimpleData(200, '用户添加成功');
    }
};

// 通过用户名查询用户信息函数
module.exports.selectUserByName = async (user_name) => {
    const result = await User.findOne({
        where: {
            user_name,
        },
    });
    if (result) {
        return SuccessData(result.get({plain: true}));
    } else {
        return SimpleData(false, '用户不存在');
    }
};

//修改用户
module.exports.modifyUser = async (params, id) => {
    const result = await User.update(
        params,
        {
            where: {
                id: id,
            },
        }
    );
    if (result[0]) {
        return SimpleData(200, '修改用户成功');
    } else {
        return SimpleData(500, '用户修改失败');
    }
};

module.exports.toggleStats = async (ids, state) => {
    let datas = ids.map(item => {
        return {id: item, state: state}
    })
    const result = await User.bulkCreate(datas, {
        updateOnDuplicate: ['state']
    })
    if (result) {
        return SimpleData(200, '修改用户成功');
    } else {
        return SimpleData(500, '用户修改失败');
    }
};


module.exports.getAllUsers = async (listQuery) => {

    // 分页
    let limit = listQuery.size
    let offset = (listQuery.current - 1) * limit

    // 查询字段
    let query = {}
    if (!!listQuery.params) {
        Object.keys(listQuery.params).forEach(key => {
            query[key] = {[Op.substring]: listQuery.params[key]}
        })
    }
    console.log(query)

    let result = await User.findAll({
        where: query,
        raw: false,
    });
    if (result) {
        let dd = result.slice(offset, offset + limit)
        return SuccessData({total: result.length, records: dd});
    } else {
        return SimpleData(500, '查询信息为空');
    }
}

// //根据id查询用户
// module.exports.findOneUser = async (user_id) => {
//   const result = (
//     await User.findOne({
//       where: {
//         user_id,
//       },
//       include: [
//         {
//           model: Topic,
//           as: 'topic',
//           include: [
//             {
//               model: Comment,
//               attributes: ['id'],
//             },
//           ],
//         },
//       ],
//       attributes: {
//         exclude: ['created_time', 'update_time'],
//       },
//     })
//   ).get({ plain: true });
//   return SuccessData(result);
// };


// //验证用户是否已经存在
// module.exports.validateUser = async (username) => {
//   const cantUpdate = await User.findOne({
//     where: {
//       username,
//     },
//     raw: true,
//   });
//   if (cantUpdate) {
//     return SimpleData(500, '该用户名已存在');
//   } else {
//     return SimpleData(200, '用户名不存在');
//   }
// };
//

// // 删除用户
// module.exports.deleteUser = async (userId) => {
//   const result = await User.destroy({
//     where: {
//       user_id: userId,
//     },
//   });
//   if (result) {
//     return SimpleData(200, '删除用户成功');
//   } else {
//     return SimpleData(500, '删除用户失败');
//   }
// };
//
// //分页查询用户
// module.exports.pagingUsers = async ({ offset, limit, username }) => {
//   if (limit) {
//     offset = (offset - 1) * limit;
//   } else {
//     offset = 0;
//   }
//   const result = await User.findAndCountAll({
//     where: {
//       username: {
//         [sequelize.Op.like]: '%' + username + '%',
//       },
//     },
//     offset,
//     limit,
//   });
//   if (result) {
//     return result;
//   } else {
//     return SimpleData(500, '获取用户列表失败');
//   }
// };
