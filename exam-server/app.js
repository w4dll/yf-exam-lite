var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
const session = require('express-session');

let app = express()

// 关联模型
require('./model/init_model')

// 日志保存
const { returnDate } = require('./common/returnDataFormat');
const fs = require('fs');
const filePath = path.join(__dirname, './logs/log.txt');
const accessLogStream = fs.createWriteStream(filePath, { flags: 'a' });

//自定义日志类型
app.use(
  morgan(
    function (tokens, req, res) {
      return [
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res),
        'ms',
        '-',
        returnDate(tokens.date(req, res)),
      ].join(' ');
    },
    { stream: accessLogStream }
  )
);

/* 跨域请求开放 */
app.all('*', function (req, res, next) {
  //设置允许跨域的域名，*代表允许任意域名跨域
  res.header('Access-Control-Allow-Origin', '*');
  //允许的header类型
  res.header('Access-Control-Allow-Headers', '*');
  //跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', 'DELETE,PUT,POST,GET,OPTIONS');

  if (req.method.toLowerCase() == 'options') res.send(200);
  //让options尝试请求快速结束
  else next();
});

// 使用session中间件
app.use(
  session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('public'));
// app.use(express.static(path.join(__dirname, 'public')));
// console.log(__dirname, '-------dirname')

/**
 * 挂载路由
 * **/

// 路由token验证
const {
  verifyUser,
  verifyAdmin,
  verifyToken,
} = require('./common/verifyToken');

const indexRouter = require('./routes/index');
app.use('/', indexRouter);

/**
 * w4dll 2022年2月9日13:00:59
 * **/
let quRouter = require('./routes/qu')
app.use('/qu', verifyToken, quRouter)




// 配置路由
const configRouter = require('./routes/config')
app.use('/config', configRouter)

// 用户路由
var usersRouter = require('./routes/users');
app.use('/user', verifyUser, usersRouter);

// 试卷路由
let paperRouter = require('./routes/paper')
app.use('/paper', verifyToken, paperRouter)

// 题库路由
let repoRouter = require('./routes/repo')
app.use('/repo', verifyToken, repoRouter)

let examRouter = require('./routes/exam')
app.use('/exam', verifyToken, examRouter)




let departRouter = require('./routes/depart')
let roleRouter = require('./routes/role')
app.use('/sys', verifyToken, departRouter, roleRouter)



app.listen(8899,() => {
  console.log('应用正在监听 8899 端口!');
})
