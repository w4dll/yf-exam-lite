const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const UserExam= sequelize.define('user_exam', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  try_count: {
    type:DataTypes.INTEGER,
  },
  max_score: {
    type:DataTypes.INTEGER,
  },
  passed: {
    type:DataTypes.INTEGER(3),
  },
},{tableName:'el_user_exam'});

module.exports = UserExam;
