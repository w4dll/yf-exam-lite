const sequelize = require('../config/sequelize')

const User = require('./userModel')
const Depart = require('./departModel')
const Exam = require('./examModel')
const Repo = require('./repoModel')
const Paper = require('./paperModel')
const PaperQu = require('./paperQuModel')
const Qu = require('./quModel')
const QuAns = require('./quAnsModel')
const QuRepo = require('./quRepoModel')
const PaperQuAns = require('./paperQuAnsModel')
const UserBook = require('./userBookModel')
const ExamRepo = require('./examRepoModel')
const UserExam = require('./userExamModel')
const Role = require('./roleModel')
const ExamDepart = require('./examDepartModel')

Role.belongsToMany(User, {through:'sys_user_role', foreignKey:'role_id'})
User.belongsToMany(Role, {through:'sys_user_role', foreignKey:'user_id'})


User.hasMany(UserExam, {foreignKey:'user_id'})
UserExam.belongsTo(User,{foreignKey:'user_id'})

Exam.hasMany(UserExam, {foreignKey:'exam_id'})
UserExam.belongsTo(Exam, {foreignKey:'exam_id'})

Exam.hasMany(UserBook, {foreignKey:'exam_id'})
User.hasMany(UserBook, {foreignKey:'user_id'})
Qu.hasMany(UserBook, {foreignKey:'qu_id'})


Qu.belongsToMany(Repo, {as:'Repo', through:'qu_repo',  foreignKey:'qu_id'})
Repo.belongsToMany(Qu, {through:'qu_repo', foreignKey:'repo_id'})
Repo.hasMany(QuRepo, {as:'QuRepos', foreignKey:'repo_id'})

Qu.hasMany(QuAns, {as:'QuAns', foreignKey:'qu_id'})
QuAns.belongsTo(Qu, {foreignKey:'qu_id'})

PaperQuAns.belongsTo(Qu, {foreignKey:'qu_id'})
PaperQuAns.belongsTo(QuAns, {foreignKey:'answer_id'})
PaperQuAns.belongsTo(Paper, {foreignKey:'paper_id'})

Paper.hasMany(PaperQuAns, {foreignKey:'paper_id'})
Qu.hasMany(PaperQuAns, {as:'PaperQuAns', foreignKey:'qu_id'})
// QuAns.belongsTo(PaperQuAns, {foreignKey:'answer_id'})

Paper.hasMany(PaperQu, {as:'PaperQus',  foreignKey: 'paper_id'})


Qu.hasMany(PaperQu, {foreignKey:'qu_id'})
PaperQu.belongsTo(Qu, {foreignKey:'qu_id'})

Exam.hasMany(ExamRepo, {foreignKey:'exam_id'})
ExamRepo.belongsTo(Exam, {foreignKey:'exam_id'})

// 一对多关系
Repo.hasMany(ExamRepo, {foreignKey:'repo_id'})
ExamRepo.belongsTo(Repo, {foreignKey:'repo_id'})

// Qu.hasMany(QuRepo, {foreignKey:'qu_id'})
// QuRepo.belongsTo(Qu, {foreignKey:'qu_id'})


Exam.belongsToMany(Depart, {through:ExamDepart, foreignKey:'exam_id'})
Depart.belongsToMany(Exam, {through:ExamDepart, foreignKey:'depart_id'})


User.hasMany(Paper, {foreignKey:'user_id'})
Paper.belongsTo(User, {foreignKey:'user_id'})

Depart.hasMany(Paper, {foreignKey:'depart_id'})
Exam.hasMany(Paper, {foreignKey:'exam_id'})

Depart.hasMany(Depart, {foreignKey:'parent_id', through:null})



// 数据同步，不强制删除，但是会修改变动内容
// const isInitDb = true
const isInitDb = false
async function initDatabase(){
  // await Depart.sync({force:true})
  await sequelize.sync({force:false, alter:true})

  // await UserExam.sync({force:true})
  // await ExamDepart.sync({force:true})
  // await sequelize.sync({force:true})
  console.log('数据库同步成功')

  /****************以下测试数据**************/
  // let res = await Repo.findAll({
  //   include:ExamRepo,
  //   raw:true,
  //   nest:true,
  // })
  // console.log(res)

}

if (isInitDb) initDatabase()
