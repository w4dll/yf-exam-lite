const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const ConfigModel = sequelize.define('config', {
  id: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey: true,
  },
  site_name: {
    type: DataTypes.STRING,
  },
  front_logo: {
    type: DataTypes.STRING,
  },
  back_logo: {
    type: DataTypes.STRING,
  },
  create_by: {
    type:DataTypes.STRING,
  },
  update_by: {
    type:DataTypes.STRING,
  },
  data_flag: {
    type:DataTypes.INTEGER,
  },
},{tableName:'sys_config'});

module.exports = ConfigModel;
