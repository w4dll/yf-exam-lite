const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const UserBook= sequelize.define('user_book', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  wrong_count: {
    type:DataTypes.INTEGER,
  },
  sort: {
    type:DataTypes.INTEGER,
  },
  title:{
    type:DataTypes.STRING,
  }
},{tableName:'el_user_book'});

module.exports = UserBook;
