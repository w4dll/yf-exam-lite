const {DataTypes} = require('sequelize')
const sequelize = require('../config/sequelize');


const Qu = sequelize.define('qu', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  qu_type: {
    type: DataTypes.INTEGER(3),
  },
  level: {
    type: DataTypes.INTEGER(3),
  },
  state: {
    type: DataTypes.INTEGER(3),
    defaultValue: 0
  },
  image: {
    type: DataTypes.STRING,
  },
  content: {
    type: DataTypes.TEXT,
  },
  remark: {
    type: DataTypes.STRING,
  },
  analysis: {
    type: DataTypes.STRING,
  }
}, {tableName: 'el_qu'});

module.exports = Qu;
