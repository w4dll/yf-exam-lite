const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const PaperQu= sequelize.define('paper_qu', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  qu_type: {
    type: DataTypes.INTEGER(3),
  },
  answered: {
    type: DataTypes.INTEGER(3),
  },
  sort: {
    type: DataTypes.INTEGER,
  },

  score: {
    type: DataTypes.INTEGER,
  },
  actual_score: {
    type:DataTypes.INTEGER,
    defaultValue:0,
  },
  is_right: {
    type: DataTypes.INTEGER,
  }
},{tableName:'el_paper_qu'});

module.exports = PaperQu;
