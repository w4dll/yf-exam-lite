const {DataTypes} = require('sequelize')
const sequelize = require('../config/sequelize');


const Role = sequelize.define('role', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    role_name: {
      type: DataTypes.STRING
    },
    desc: {
      type: DataTypes.STRING
    }
  },
  {tableName: 'sys_role'});

module.exports = Role
