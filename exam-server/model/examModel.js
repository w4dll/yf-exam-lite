const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');

const Sequelize = require('sequelize')


const Exam = sequelize.define('exam', {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4 // 或 Sequelize.UUIDV1
  },
  title: {
    type: DataTypes.STRING,
  },
  content: {
    type: DataTypes.STRING,
  },
  open_type: {
    type: DataTypes.INTEGER(3),
  },
  join_type: {
    type:DataTypes.INTEGER(3),
  },
  level: {
    type:DataTypes.INTEGER(3),
  },
  state: {
    type:DataTypes.INTEGER(3),
    defaultValue: 0
  },
  time_limit: {
    type:DataTypes.INTEGER(3),
  },
  start_time: {
    type:DataTypes.DATEONLY,
  },
  end_time: {
    type:DataTypes.DATEONLY,
  },
  total_score: {
    type: DataTypes.INTEGER,
  },
  total_time: {
    type: DataTypes.INTEGER,
  },
  qualify_score: {
    type: DataTypes.INTEGER,
  }
},{tableName:'el_exam'});

module.exports = Exam;
