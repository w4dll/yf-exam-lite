const {DataTypes} = require('sequelize')
const sequelize = require('../config/sequelize');


const PaperQuAns = sequelize.define('paper_qu_ans', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  is_right: {
    type: DataTypes.INTEGER(3),
  },
  checked: {
    type: DataTypes.INTEGER(3),
    defaultValue:0,
  },
  sort: {
    type: DataTypes.INTEGER,
  },
  abc: {
    type: DataTypes.STRING,
  },
}, {tableName: 'el_paper_qu_answer'});

module.exports = PaperQuAns;
