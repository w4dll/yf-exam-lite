const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const QuRepo = sequelize.define('qu_repo', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  qu_type: {
    type: DataTypes.INTEGER(3),
  },
  sort: {
    type: DataTypes.INTEGER,
  },
},{
  tableName:'el_qu_repo',
  timestamps:false,
});

module.exports = QuRepo;
