const { DataTypes, Sequelize } = require('sequelize')
const sequelize = require('../config/sequelize');



const ExamDepart = sequelize.define('exam_depart', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement:true,
    allowNull: false,
    primaryKey: true,
  },
}, {
  tableName:'el_exam_depart'
})

module.exports = ExamDepart;
