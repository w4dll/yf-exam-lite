const { DataTypes, Sequelize } = require('sequelize')
const sequelize = require('../config/sequelize');



const ExamRepo = sequelize.define('exam_repo', {
  id: {
    // 随机生成一个UUID类型的识别码作为id，设置类型后必须配置默认值
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,

    allowNull: false,
    primaryKey: true,
  },
  radio_count: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  radio_score: {
    type:DataTypes.INTEGER,
    defaultValue: 0,
  },
  multi_count: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  multi_score: {
    type:DataTypes.INTEGER,
    defaultValue: 0,
  },
  judge_count: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  judge_score: {
    type:DataTypes.INTEGER,
    defaultValue: 0,
  },
  saq_count: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  saq_score: {
    type:DataTypes.INTEGER,
    defaultValue: 0,
  },
},{tableName:'el_exam_repo'});

module.exports = ExamRepo;
