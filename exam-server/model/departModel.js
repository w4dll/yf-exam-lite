const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const Depart= sequelize.define('depart', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  parent_id:{
    type:DataTypes.STRING,
  },
  depart_type: {
    type: DataTypes.INTEGER,
  },
  depart_name: {
    type: DataTypes.STRING,
  },
  depart_code: {
    type: DataTypes.STRING,
  },
  sort: {
    type:DataTypes.INTEGER,
  },
},{tableName:'sys_depart'});

module.exports = Depart;
