const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const QuAns= sequelize.define('qu_ans', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  is_right: {
    type: DataTypes.INTEGER(3),
  },
  image: {
    type: DataTypes.STRING,
  },
  content: {
    type: DataTypes.STRING,
  },
  analysis: {
    type: DataTypes.STRING,
  },
},{tableName:'el_qu_answer'});

module.exports = QuAns;
