const {DataTypes} = require('sequelize')
const sequelize = require('../config/sequelize');

const Repo = sequelize.define('repo', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  code:DataTypes.STRING,
  title:DataTypes.STRING,
  remark:DataTypes.STRING,
  multi_count:{
    type: DataTypes.INTEGER,
    defaultValue:0
  },
  radio_count:{
    type: DataTypes.INTEGER,
    defaultValue:0
  },
  judge_count:{
    type: DataTypes.INTEGER,
    defaultValue:0
  },
},{
  tableName:'el_repo'
})

module.exports = Repo;
