const { DataTypes, Sequelize } = require('sequelize')
const sequelize = require('../config/sequelize');


const Paper = sequelize.define('paper', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  title: {
    type: DataTypes.STRING,
  },
  total_time: {
    type:DataTypes.INTEGER,
  },
  user_time: {
    type: DataTypes.INTEGER,
  },
  total_score: {
    type:DataTypes.INTEGER,
  },
  qualify_score: {
    type: DataTypes.INTEGER,
  },
  user_score: {
    type:DataTypes.INTEGER,
  },
  subj_score: {
    type: DataTypes.INTEGER,
  },
  has_saq: {
    type:DataTypes.INTEGER(3),
  },
  state: {
    type:DataTypes.INTEGER(3),
  },
  limit_time: {
    type:DataTypes.DATE
  }
},{tableName:'el_paper'});

module.exports = Paper;
