const { DataTypes } = require('sequelize')
const sequelize = require('../config/sequelize');


const User= sequelize.define('user', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement:true
  },
  user_name: {
    type: DataTypes.STRING,
  },
  real_name: {
    type: DataTypes.STRING,
  },
  password: {
    type: DataTypes.STRING,
  },
  salt: {
    type:DataTypes.STRING,
  },
  role_ids: {
    type:DataTypes.STRING,
  },
  depart_id: {
    type:DataTypes.STRING,
  },
  avatar: {
    type:DataTypes.STRING,
  },
  state: {
    type:DataTypes.INTEGER,
  },
  data_flag: {
    type:DataTypes.INTEGER,
  },
},{tableName:'sys_user'});

module.exports = User;
