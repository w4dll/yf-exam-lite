import { post } from '@/utils/request'

/**
 * 保存题库
 * @param data
 */
// export function saveData(data) {
//   return post('/repo/save', data)
// }

/**
 * 获取题库信息
 * @param data
 */
export function fetchList(data) {
  return post('/repo/list', data)
}

/**
 * 题库详情
 * @param data
 */
// export function fetchDetail(data) {
//   return post('/exam/repo/detail', data)
// }

// /**
//  * 题库批量操作
//  * @param data
//  */
// export function batchAction(data) {
//   return post('/exam/api/repo/batch-action', data)
// }
