import { post } from '@/utils/request'

/**
 * 题库详情
 * @param examId
 * @param quId
 */
export function nextQu(examId, quId) {
  return post('/qu/wrong-book/next', { examId: examId, quId: quId })
}

