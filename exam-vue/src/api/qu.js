import { post, upload, download } from '@/utils/request'

/**
 * 保存题库
 * @param data
 */
export function saveData(data) {
  return post('/qu/save', data)
}

/**
 * 获取指定题号题目信息
 * @param id
 */
export function fetchDetail(id) {
  return post('/qu/detail', { id: id })
}

/**
 * 获取指定题号题目信息
 * @param exam_id
 */
export function fetchWrongQus(exam_id) {
  return post('/qu/wrong-qus', { id: exam_id })
}

/**
 * 导入模板
 */
export function importTemplate() {
  return download('/qu/import/template', {}, 'qu-import-template.xlsx')
}

/**
 * 导出
 * @param file
 */
export function importExcel(file) {
  return upload('/qu/import/excel', file)
}

/**
 * 导出
 * @param data
 */
export function exportExcel(data) {
  return download('/qu/export', data, '导出的数据.xlsx')
}
