import { post } from '@/utils/request'

export function login(data) {
  return post('/user/login', data)
}

export function getInfo() {
  return post('/user/info')
}

export function logout() {
  return post('/user/logout', {})
}

export function reg(data) {
  return post('/user/reg', data)
}
