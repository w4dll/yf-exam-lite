import { post } from '@/utils/request'

export function pagingTree(data) {
  return post('/sys/depart/paging', data)
}

export function fetchAll(data) {
  return post('/sys/depart/all', data)
}

export function saveData(data) {
  return post('/sys/depart/save', data)
}

export function fetchDetail(id) {
  const data = { id: id }
  return post('/sys/depart/detail', data)
}

export function fetchTree(data) {
  return post('/sys/depart/tree', data)
}

/** ...以上为修改过得内容... **/

export function deleteData(ids) {
  const data = { ids: ids }
  return post('/exam/api/sys/depart/delete', data)
}

export function sortData(id, sort) {
  const data = { id: id, sort: sort }
  return post('/exam/api/sys/depart/sort', data)
}
