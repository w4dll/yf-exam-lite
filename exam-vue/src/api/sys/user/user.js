import { post } from '@/utils/request'

export function updateData(data) {
  return post('/user/update', data)
}

export function saveData(data) {
  return post('/user/save', data)
}

export function userReg(data) {
  return post('/exam/api/sys/user/reg', data)
}
