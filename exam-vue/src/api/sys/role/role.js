import { post } from '@/utils/request'

export function fetchList() {
  return post('/sys/role/list', {})
}

export function addRole(params) {
  return post('/sys/role/add', params)
}
