import { post } from '@/utils/request'

import request from '@/utils/request'

// 获得用户协议详情，固定ID为0
export function fetchDetail() {
  return request({
    url: '/config/detail',
    method: 'post'
  })
}

export function saveData(data) {
  return post('/config/save', data)
}
