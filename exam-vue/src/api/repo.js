import { post } from '@/utils/request'

/**
 * 获取题库信息
 * @param data
 */
export function fetchList(data) {
  return post('/repo/list_all', data)
}

/**
 * 题库批量操作
 * @param data
 */
export function batchAction(data) {
  return post('/repo/batch-action', data)
}

/**
 * 保存题库
 * @param data
 */
export function saveData(data) {
  return post('/repo/save', data)
}

/**
 * 题库详情
 * @param data
 */
export function fetchDetail(data) {
  return post('/repo/detail', data)
}
