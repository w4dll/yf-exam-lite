import { post } from '@/utils/request'

/**
 * w4dll 2022年2月13日10:37:15
 * 试卷列表
 * @param userId
 * @param examId
 */
export function listPaper(userId, examId) {
  return post('/paper/listUserExam', { current: 1, size: 5, params: { user_id: userId, exam_id: examId }})
}

export function listUserPaper(userId, examId) {
  return post('/paper/listUserPaper', { user_id: userId, exam_id: examId })
}

/**
 * w4dll 2022年2月13日10:37:28
 * 创建试卷
 * @param data
 */
export function createPaper(data) {
  return post('/paper/create-paper', data)
}

/**
 * w4dll 2022年2月13日10:45:22
 * 试卷详情
 * @param data
 */
export function paperDetail(data) {
  return post('/paper/paper-detail', data)
}

/**
 * 题目详情
 * @param data
 */
export function quDetail(data) {
  return post('/paper/qu-detail', data)
}

/**
 * 填充答案
 * @param data
 */
export function fillAnswer(data) {
  return post('/paper/fill-answer', data)
}

/**
 * 交卷
 * @param data
 */
export function handExam(data) {
  return post('/paper/hand-exam', data)
}

/**
 * 试卷详情
 * @param data
 */
export function paperResult(data) {
  return post('/paper/paper-result', data)
}

/**
 * 获取考试所有错题信息
 * **/
export function wrongExams(examId) {
  return post('/paper/wrong-exams', { exam_id: examId })
}
