import { post } from '@/utils/request'

/**
 * 考试信息
 * @param id
 */
export function fetchDetail(id) {
  return post('/exam/detail', { id: id })
}

/**
 * 保存题库
 * @param data
 */
export function saveData(data) {
  return post('/exam/save', data)
}

